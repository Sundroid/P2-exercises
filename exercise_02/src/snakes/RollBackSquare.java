package snakes;
/*
 * Landing on this type of square is like landing on a regular square
 * If a player is on this square a roll a number the player goes back the number that he rolled
 * 
 *  
 */
public class RollBackSquare extends Square {
	public RollBackSquare(Game game, int position) {
		super(game, position);
	}

	@Override
	public String squareLabel() {
		return String.format("%d (RollBack)", position);
	}

	@Override
	public ISquare moveAndLand(int moves) {
		return super.moveAndLand(moves * -1);
	}
}
