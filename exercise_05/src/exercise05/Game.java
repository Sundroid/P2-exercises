package exercise05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a Quoridor game. Checks if a player has wins Manages turns and
 * communication between tiles and players
 */
public class Game implements IGame {
    private Player currentPlayer;
    private int currentPlayerIndex;
    final private int height;
    final private int width;
    final private Tile[][] tiles;
    final protected List<Player> players;

    public Game(int widthSet, int heightSet, List<String[]> players) throws PlayerException {
        this(widthSet, heightSet, players, new BufferedReader(new InputStreamReader(System.in)));
    }

    /**
     * Creates the Game.Called from the parser after reading file
     *
     * @param widthSet      Width of the playing field left and right side are
     *                      automatically set to walls so the width of the game is +2 of
     *                      the widthSet
     * @param heightSet     Height of the game upper and lower boarder are automatically
     *                      set to walls so the height is +2 of the heightSet
     * @param playerStrings Players that need to be created by the Player constructer test if the
     *                      players are correctly defined
     * @param input         Buffered Reader which any input of the players will be read
     * @throws PlayerException if the player could not be created correctly
     */
    public Game(int widthSet, int heightSet, List<String[]> playerStrings,BufferedReader input)
            throws PlayerException {
        this.width = widthSet + 2;
        this.height = heightSet + 2;
        tiles = new Tile[width][height];
        initializeTilesandWalls();
        initializeTilesandWalls();
        this.players = createPlayerList(playerStrings, input, 5, null);// }
        initializePlayersOnField();
        currentPlayer = getPlayers().get(0);
        currentPlayerIndex = 0;
    }
    /**
     * Creates the game out of a file in "new Format"
     * For Dependency Injection
     * @param usages Describes where the characters in sign are used 1 when used 0 else
     * @param signs which appeared on the board in your file
     * @param width of the game
     * @param height of the game
     * @param players List of player strings 
     * @param number tells how many walls the players may place
     * @throws PlayerException
     */
    public Game(List<int[][]> usages, List<Character> signs, int width2, int height2, List<String[]> players2,
                BufferedReader input, List<Integer> number) throws PlayerException {
        height = height2;
        width = width2;
        tiles = new Tile[width][height];
        this.players = this.createPlayerList(players2, input, 2, number);
        initializeBoard(usages, signs);
        currentPlayer = getPlayers().get(0);
        currentPlayerIndex = 0;
    }
    /**
     * Creates the game out of a file in "new Format"
     * For usage with ServiceLocator
     * @param usages Describes where the characters in sign are used 1 when used 0 else
     * @param signs which appered on the board in your file
     * @param width of the game
     * @param height of the game
     * @param players List of player strings 
     * @param number tells how many walls the players may place
     * @throws PlayerException
     */
    public Game(List<int[][]> usages, List<Character> signs, int width, int height, List<String[]> players2,
    			List<Integer> number) throws PlayerException {
        this.height = height;
        this.width = width;
        tiles = new Tile[width][height];
        this.players = this.createPlayerList(players2, ServiceLocator.instance().getInput(), 2, number);
        initializeBoard(usages, signs);
        currentPlayer = getPlayers().get(0);
        currentPlayerIndex = 0;
    }

    private void initializeBoard(List<int[][]> usages, List<Character> signs) {
        assert (signs.contains('#'));
        int i = signs.indexOf('#');
        placeWalls(usages.get(i));

        if (signs.contains(' ')) {
            i = signs.indexOf(' ');
            placeEmptyTiles(usages.get(i));

        }

        for (Player player : getPlayers()) {
            assert (signs.contains(player.getSign().charAt(0))) : player;
            i = signs.indexOf(player.getSign().charAt(0));
            placePlayer(usages.get(i), player);
            assert (signs.contains(player.getSign().toLowerCase().charAt(0)));
            i = signs.indexOf(player.getSign().toLowerCase().charAt(0));
            placeGoalTiles(usages.get(i), player);

        }

    }

    private void placeEmptyTiles(int[][] is) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (is[j][i] == 1) {
                    tiles[i][j] = new Tile(false, i, j, this);
                }
            }
        }
    }

    private void placeWalls(int[][] is) {
        assert is.length == height;
        assert is[0].length == width;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {

                if (is[j][i] == 1) {
                    tiles[i][j] = new Tile(true, i, j, this);
                }


            }
        }

    }

    private void placeGoalTiles(int[][] is, Player player) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (is[j][i] == 1) {
                    tiles[i][j] = new Tile(false, i, j, this, player.getSign().toLowerCase());
                    player.addWinningTile(tiles[i][j]);
                }

            }
        }

    }

    private void placePlayer(int[][] is, Player player) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (is[j][i] == 1) {
                    tiles[i][j] = new Tile(false, i, j, this);
                    tiles[i][j].enter(player);
                    player.setPositionX(i);
                    player.setPositionY(j);
                }

            }
        }

    }


    /* (non-Javadoc)
	 * @see exercise05.IGame#toString()
	 */
    @Override
    public String toString() {
        return "Game [currentPlayer=" + currentPlayer + ", height=" + height + ", width=" + width + ", tiles="
                + Arrays.toString(tiles) + ", players=" + getPlayers() + "]";
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#hashCode()
	 */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((currentPlayer == null) ? 0 : currentPlayer.hashCode());
        result = prime * result + height;
        result = prime * result + ((getPlayers() == null) ? 0 : getPlayers().hashCode());
        result = prime * result + Arrays.deepHashCode(tiles);
        result = prime * result + width;
        return result;
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#equals(java.lang.Object)
	 */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Game other = (Game) obj;
        if (currentPlayer == null) {
            if (other.currentPlayer != null) {
                return false;
            }
        } else if (!currentPlayer.equals(other.currentPlayer)) {
            return false;
        }
        if (height != other.height) {
            return false;
        }
        if (getPlayers() == null) {
            if (other.getPlayers() != null) {
                return false;
            }
        } else if (!getPlayers().equals(other.getPlayers())) {
            return false;
        }

        if (!Arrays.deepEquals(tiles, other.tiles)) {
            return false;
        }
        if (width != other.width) {
            return false;
        }
        return true;
    }

    /**
     * Initializes Tiles so that all tiles are empty except the bottom top left
     * and right boarder are Walls Called from Game constructor
     */
    private void initializeTilesandWalls() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if ((j > 0 && j < width - 1) && i > 0 && i < height - 1) {
                    tiles[j][i] = new Tile(false, j, i, this);
                } else {
                    tiles[j][i] = new Tile(true, j, i, this);
                }

            }
        }
    }

    /**
     * Sets all players to their starting position
     */
    private void initializePlayersOnField() {
        for (Player player : getPlayers()) {
            currentPlayer = player;
            enterPlayerOnTile(player.getPositionX(), player.getPositionY());
        }
    }

    /**
     * Creates the players that are specified Called from game constructor
     *
     * @param Players Specifies the players you want to create.
     * @param inputs
     * @return PlayerList that have been created
     * @throws PlayerException
     */
    private List<Player> createPlayerList(List<String[]> Players,BufferedReader input, int number, List<Integer> numbers) throws PlayerException {
        List<Player> newPlayers = new ArrayList<Player>();
        int walls = 0;
        for (String[] player : Players) {
            if (number == 5) {
                walls = 5;
            } else {
                walls = numbers.get(Players.indexOf(player));
            }
            newPlayers.add(new Player(player, this, input, walls));

        }
        return newPlayers;
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#getTileContent(int, int)
	 */
    @Override
	public String getTileContent(final int x, final int y) {
        assert y >= 0 && y < height;
        String s = tiles[x][y].getContent();
        assert s.length() == 1;
        return s;
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#getHeight()
	 */
    @Override
	public int getHeight() {
        return height;
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#getWidth()
	 */
    @Override
	public int getWidth() {

        return width;
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#enterPlayerOnTile(int, int)
	 */
    @Override
	public void enterPlayerOnTile(int x, int y) {
        assert x >= 1 && x < width - 1;
        assert y >= 1 && y < height - 1;
        tiles[x][y].enter(currentPlayer);
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#isValidPosition(int, int)
	 */
    @Override
	public boolean isValidPosition(int x, int y) {
        if (x < 0 || x > width - 1) {
            return false;
        }
        if (y < 0 || y > height - 1) {
            return false;
        }
        return tiles[x][y].canBeEntered();
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#getTile(int, int)
	 */
    @Override
	public Tile getTile(int x, int y) {
        assert x >= 0 && x < width;
        assert y >= 0 && y < height;
        return tiles[x][y];
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#checkIfvalidPathsExists()
	 */
    @Override
	public boolean checkIfvalidPathsExists() {
        for (Player player : getPlayers()) {
            if (!canReachGoal(player)) {
                return false;
            }

        }
        return true;
    }

    /**
     * Checks if this player wins if he would be on this position
     *
     * @param player
     * @param x      xPosition
     * @param y      yPosition
     * @return true when player would win
     */
    protected boolean IsOver(Player player, int x, int y) {
        if (player.isWinningTile(getTile(x, y)) == true) {
            return true;
        }
        return false;
    }

    /**
     * Checks if the player can reach his goal side
     *
     * @param player which is checked if he can reached the end
     * @return false when goal is blocked even temporarily
     */
    private boolean canReachGoal(Player player) {
        int x = player.getPositionX();
        int y = player.getPositionY();
        List<Tile> tilesVisited = new ArrayList<Tile>();
        Tile currentTile = tiles[x][y];
        int i = 0;
        tilesVisited.add(currentTile);

        while (i < tilesVisited.size()) {
            if (IsOver(player, currentTile.getX(), currentTile.getY())) {
                return true;
            }

            currentTile = tilesVisited.get(i);
            if (currentTile.canBeEntered() || i == 0) {

                for (Tile tile : currentTile.adjacentTiles()) {
                    if (!tilesVisited.contains(tile)) {
                        tilesVisited.add(tile);
                    }
                }

            }
            i++;
        }

        return false;
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#getSideTiles(java.lang.String)
	 */
    @Override
	public List<Tile> getSideTiles(String goalSide) throws PlayerException {
        List<Tile> sideTiles = new ArrayList<Tile>();
        int boarder = 0;
        int upAndDownMove = 0;
        int r = 1;
        int rL = 0;
        int d = 1;
        switch (goalSide) {
            case "R":
                rL = 1;
                r = width - 2;
                break;
            case "L":
                rL = 1;
                break;
            case "U":
                upAndDownMove = 1;
                break;
            case "D":
                upAndDownMove = 1;
                d = height - 2;
                break;
            default:
                throw new PlayerException("No correct goal side");
        }
        if (upAndDownMove == 0) {
            boarder = height - 2;
        } else {
            boarder = width - 2;
        }
        for (int i = 0; i < boarder; i++) {
            sideTiles.add(tiles[r + upAndDownMove * i][d + rL * i]);
        }
        return sideTiles;
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#setWall(int, int)
	 */
    @Override
	public void setWall(int x, int y) {
        assert x >= 0 && x < width;
        assert y >= 0 && y < height;
        tiles[x][y].setToWall();
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#unsetWall(int, int)
	 */
    @Override
	public void unsetWall(int x, int y) {
        assert x >= 0 && x < width;
        assert y >= 0 && y < height;
        tiles[x][y].unsetToWall();
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#IsOver()
	 */
    @Override
	public boolean IsOver() {

        return IsOver(currentPlayer, currentPlayer.getPositionX(), currentPlayer.getPositionY());
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#nextPlayer()
	 */
    @Override
	public void nextPlayer() {
        currentPlayerIndex = (currentPlayerIndex + 1) % getPlayers().size();
        currentPlayer = getPlayers().get(currentPlayerIndex);
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#nextTurn(exercise05.UserInteraction)
	 */
    @Override
	public void nextTurn(UserInteraction interactor) {

        interactor.interact(currentPlayer, this);
        if (!IsOver()) {
            nextPlayer();

        }
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#nextTurn()
	 */
    @Override
	public void nextTurn() throws PlayerException, IOException {
        currentPlayer.makeMove();
        if (!IsOver()) {
            nextPlayer();
        }
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#getCurrentPlayer()
	 */
    @Override
	public Player getCurrentPlayer() {
        return currentPlayer;
    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#play(exercise05.UserInteraction)
	 */
    @Override
	public void play(UserInteraction interactor) {
        while (!IsOver()) {
            nextTurn(interactor);
        }

    }

    /* (non-Javadoc)
	 * @see exercise05.IGame#getPlayers()
	 */
    @Override
	public List<Player> getPlayers() {
        return players;
    }
}
