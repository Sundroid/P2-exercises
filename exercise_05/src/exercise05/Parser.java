package exercise05;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Parses a Quoridor file specification and creates a {@link Game} instance.
 */
public class Parser {

    public List<String[]> players;
    private int height;
    private int width;

    protected List<Character> signs;

    /**
     * usages stores a height times width arrays
     * the i-th array in usages represents where
     * the i-th sign from signs is used
     * an entry will be 1 if this sign is on this position
     * of the board part of a command and 0 otherwise;
     */

    protected ArrayList<int[][]> usages;
    private ArrayList<Integer> numbers;

    /**
     * Creates the Parser with an empty Player List
     */
    public Parser() {
        players = new ArrayList<String[]>();
    }


    public Game parse(final BufferedReader br, final BufferedReader input) throws IOException, PlayerException {
        String firstLine = br.readLine();
        assert firstLine != null;
        if (firstLine.length() <= 4) {
            return parseOldFormat(br, input, firstLine);
        } else {
            return parseNewFormat(br, input, firstLine);
        }

    }



    /**
     * Creates a game out of this BufferedReader Reset the whole Parser The
     * Parser will only work if your input is as follows First line int int
     * Every other line: 5 or more strings separated by " " Return null game if
     * a specification of game or player is not meet
     *
     * @param br Reader which contains your game description Will be read to
     *           the end and closed.
     * @return Game which was created from this input
     * @throws PlayerException if the game could not be parsed correctly
     */
    private Game parseOldFormat(final BufferedReader br, final BufferedReader input, final String firstLine) throws IOException, PlayerException {
        height = 0;
        width = 0;
        players = new ArrayList<String[]>();
        signs = new ArrayList<Character>();

        Game game;
        String s;
        int i = 0;

        getHeightandWidth(firstLine);
        i++;

        while ((s = br.readLine()) != null) {
            if (i >= 1) {
                parsePlayer(s, 5);
            }
            i++;
        }

        game = new Game(width, height, players, input);

        br.close();
        return game;
    }

    /**
     * Creates a game out of your bufferedReader
     * In difference to the parse method the format works as
     * follow first the measurements of the board in notation "x y"
     * on the first line will follow additional numbers stating how many
     * walls each player is allowed to placed
     * Following that the board with "#" as Walls " " as empty space
     * capital letters as Player positions and lower case as their finishing positions
     * afterward all Players with their signature in their full name are listed
     *
     * @param br    the Input that follow the above mentioned notation
     * @param input will be used by the player as a way of getting new command
     * @return a new game as described with the br BufferedReader
     * @throws IOException
     * @throws PlayerException if anything is not exactly how it should be according to the describtion
     */

    private Game parseNewFormat(final BufferedReader br, final BufferedReader input, final String firstLine) throws IOException, PlayerException {
        height = 1;
        width = 0;
        players = new ArrayList<String[]>();
        signs = new ArrayList<Character>();
        usages = new ArrayList<int[][]>();
        numbers = new ArrayList<Integer>();
        Game game;
        String s;
        int i = 0;

        getHeightandWidth(firstLine);
        getNumber(firstLine);
        i++;

        while ((s = br.readLine()) != null) {
            if (i <= height) {
                checkForNewSign(s);
                findUsageOfSigns(s, i);
                i++;
                continue;
            }
            parsePlayer(s, 2);
        }

        
        game=new Game(getUsages(),getSigns(),width,height,players,input,numbers);
    
         
        assert game != null;
        br.close();
        return game;
    }

    /**
     * Finds for all signs where they are used in string
     *
     * @param s          this string is searched
     * @param lineNumber Specifies on which line of the game you are
     *                   this is important so usages can be modified correctly
     */
    private void findUsageOfSigns(String s, int lineNumber) {
        for (int i = 0; i < getSigns().size(); i++) {
            findCharacterInString(s, getSigns().get(i), lineNumber, i);
        }
    }

    /**
     * Parses height and Width out of the numbers on the first line
     *
     * @param s the first line of the bufferedReader given to parse
     */
    private void getHeightandWidth(String s) {
        List<String> numbers = splitOnSpace(s);

        // assert numbers.size() == 2:s;
        width = Integer.parseInt(numbers.get(0));
        height = Integer.parseInt(numbers.get(1));
    }

    /**
     * Splits a String on spaces and than parses it into numbers
     * these are stored in the numbers field
     *
     * @param s This is split and parsed to numbers
     */
    private void getNumber(String s) {
        List<String> String = splitOnSpace(s);

        for (String number : String) {
            numbers.add(Integer.parseInt(number));
        }

    }

    /**
     * Parses players out of the second to last line of your br
     *
     * @param s the second to last line of the bufferedReader given to parse
     */
    public void parsePlayer(String s, int number) {
        List<String> player = splitOnSpace(s);
        assert player.size() >= number;
        String[] rplayer = reduceToCharacters(player, number);
        players.add(rplayer);

    }

    /**
     * Reduces this list length to number elements and returns it as a String[] All
     * elements except the last 4 will be combined together and stored in
     * String[0]
     * This method call reduceStringArrayToLength2 if number==2
     * Their the first element of the list is left out and the rest concatenated
     *
     * @param player Should be more than number or equal elements long
     * @return String[] of exactly number elements
     */
    private String[] reduceToCharacters(List<String> player, int number) {
        int j = player.size();

        if (number == 2) {
            return reduceStringArrayToLength2(player);
        }

        String s = "";
        String[] rPlayer = new String[number];
        for (int i = 0; i <= j - number; i++) {
            s += player.get(i) + " ";
        }
        s = s.trim();
        rPlayer[0] = s;
        for (int i = 1; i < number; i++) {
            rPlayer[i] = player.get(i + (j - number));

        }
        return rPlayer;
    }

    /**
     * Reduces List<String> into a new String array with length==2
     * the first Element being the same as before and all the Rest
     * concatenated and added " " in between all Elements into the second one
     *
     * @param strings this list will be reduced
     * @return String[] containing all the elements of the List but with the first one in the first position
     * and the rest in the second one
     */
    private String[] reduceStringArrayToLength2(List<String> strings) {
        int j = strings.size();
        String s = "";
        String[] rPlayer = new String[2];
        rPlayer[0] = strings.get(0);

        for (int i = 1; i < j; i++) {
            s += strings.get(i) + " ";
        }
        s.trim();
        rPlayer[1] = s;
        assert rPlayer.length == 2;
        return rPlayer;
    }

    /**
     * Split your String on Space and returns it as a list
     *
     * @param s Will be split on every space character
     * @return List Elements are in the order of the original string
     */
    protected static List<String> splitOnSpace(String s) {
        String[] word = s.split("\\s");
        assert word.length > 0;
        return Arrays.asList(word);

    }

    /**
     * Find where in a given string a certain character is if at all
     *
     * @param s      String you want your Char to be found in
     * @param sign   The Char you want to find
     * @param start  Equals the beginning of the current line of the game
     * @param number Current position in the current line
     */
    protected void findCharacterInString(String s, char sign, int start, int number) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == sign) {
                assert (i < width) : i + " " + start + " " + width + " " + height;
                assert (start - 1 < height);
                getUsages().get(number)[start - 1][i] = 1;
            }
        }

    }

    /**
     * Checks in a String if a character appears that is not already in the signs list
     *
     * @param s Here you want to look for new Characters
     */
    protected void checkForNewSign(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!getSigns().contains(s.charAt(i))) {
                getSigns().add(s.charAt(i));
                getUsages().add(new int[height][width]);
            }
        }

    }

    /**
     * Returns the List of Signs from the parser.  Mainly for testing purposes.
     *
     * @return the "signs" ArrayList of the Parser.
     */

    public List<Character> getSigns() {
        return signs;
    }


    /**
     * Returns the @usages ArrayList of the Parser. Mainly for testing purposes.
     *
     * @return returns the "usages" Array of the Parser.
     */
    public ArrayList<int[][]> getUsages() {
        return usages;
    }

}
