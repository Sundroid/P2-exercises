package snakes;

import java.util.List;
import java.util.Random;
/*
 * Landing on this square the player is transported to a random Wormhole Exit.
 *
 */
public class WormholeEntrance extends Square {

	public WormholeEntrance(Game game, int position) {
		super(game, position);
	}
	@Override
	public ISquare landHereOrGoHome(){
		Random random =new Random();
		List<ISquare> List = game.wormholeExits();
		return List.get(random.nextInt(List.size())).landHereOrGoHome();
	}
	@Override
	public String squareLabel() {
		return Integer.toString(position)+" (Entrance)";
	}

}