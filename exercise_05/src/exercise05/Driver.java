package exercise05;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 * The main class that starts the parser and executes the game Calls the
 * renderer when needed
 *
 * @author Tobias
 */
public class
Driver {
	PrintStream out;
	UserInteraction userInteractor;
	IGame game;
	/**
	 *	Construction with ServiceLocator
	 * @param game The game that will be played
	 * @param out Where Driver puts its output
	 * @param userInteractor Determines how the users are notified
	 */
	public Driver(){
		out=ServiceLocator.instance().getOutput();
		userInteractor=ServiceLocator.instance().getUserInteractor();
		game=ServiceLocator.instance().getIGame();
	}
	/**
	 * Dependency Injection
	 * @param game The game that will be played
	 * @param out Where Driver puts its output
	 * @param userInteractor Determines how the users are notified
	 */
	public Driver(IGame game,PrintStream out,UserInteraction userInteractor){
		this.out=out;
		this.userInteractor=userInteractor;
		this.game=game;
		
		
	}
	
    public static void main(String[] args) throws IOException {
    	
    	
    	Driver driver=new Driver();
    	
    	/*IGame game=createIGameFromFile();
    	UserInteraction interact=new UserInteraction(System.out);
    	Driver driver=new Driver(game,System.out,interact);*/
    	driver.play();
    	
    }
    public void play() throws IOException{
    	
        out.println(Renderer.draw(game));
        out.println("To move type \"D\"own, \"U\"p, \"L\"eft or \"R\"ight and hit enter.\n");
        out.println("To place a tile, type the X- and Y-Coordinates of the first part and then the X- and Y-Coordinates of the second part \n" +
                "For example \"1 1 2 1\" will place a horizontal tile in the top-left corner.\n");
      
        game.play(userInteractor);
        out.println("Winner is: " + game.getCurrentPlayer().getName());	
    }
    protected static IGame createIGameFromFile() throws IOException {
        System.out.println("Enter a valid game name from the games folder and hit enter:");

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        String fileName = "games/" + input.readLine();
        File gameFile = new File(fileName);
        while (!gameFile.exists() || gameFile.isDirectory()) {
            System.out.println("Sorry, could not find a game with the specified name!\nTry again: ");
            fileName = "games/" + input.readLine();
            gameFile = new File(fileName);
        }
        IGame game;
        BufferedReader br = new BufferedReader(new FileReader(fileName));

        Parser parser = new Parser();
        while (true) {
            try {
                game = parser.parse(br, input);
                break;
            } catch (Exception e) {
                System.out.println("No game was parsable\nExit!");
                System.exit(0);
            }
        }
        return game;
    }
}
