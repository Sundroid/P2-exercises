import exercise_10.Batch;
import exercise_10.Book;
import exercise_10.BookOrder;
import exercise_10.Shop;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static exercise_10.Shop.shop;
import static org.junit.Assert.assertEquals;

/**
 * Integration tests cover, as opposed to unit tests, parts of or even the whole system.
 * They can be used to test scenarios of the full working system and complement unit tests.
 * Often, integration tests are more "high level" and tests the overall behaviour of the system.
 * <p>
 * This class contains some usage scenarios and can be a start for designing your implementation.
 * However, you do not need to keep everything the same, you are free to add, remove, and modify classes
 * and interfaces.
 */
public class IntegrationTests {

    Book patterns;
    public static Batch p2, p1, mainOrder, oop, fp, software, cookingBooks, travelBooks, europe, asia, shop;


    @Before
    public void initialize() {
        Calendar cDesignPatterns = Calendar.getInstance();
        cDesignPatterns.set(1994, Calendar.OCTOBER, 31);
        Book designPatterns = new Book("Design patterns : elements of reusable object-oriented software", "Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides", "Addison Wesley", "Software Engineering", 416, 40.2, cDesignPatterns);

        Calendar cCleanCode = Calendar.getInstance();
        cCleanCode.set(2008, Calendar.AUGUST, 01);
        Book cleanCode = new Book("Clean Code: A Handbook of Agile Software Craftsmanship", "Robert C. Martin", "Prentice Hall", "Software Engineering", 464, 50, cCleanCode);

        Calendar cIfp = Calendar.getInstance();
        cIfp.set(1998, Calendar.APRIL, 29);
        Book ifp = new Book("Introduction Functional Programming", "Richard Bird, Philip Wadler", "Prentice Hall", "Software Engineering", 448, 112.74, cIfp);

        Calendar cRwfp = Calendar.getInstance();
        cRwfp.set(2010, Calendar.JANUARY, 25);
        Book rwfp = new Book("Real-World Functional Programming", "Tomas Petricek, Jon Skeet", "Manning Publications", "Software Engineering", 500, 35.16, cRwfp);

        Calendar generic = Calendar.getInstance();
        generic.set(1970, Calendar.JANUARY, 01);

        Book pharo = new Book("Pharo by Example", "Hans", "Fritz", "Software Engineering", 10, 19, generic);


        europe = new Batch("Europe");
        asia = new Batch("Asia");
        travelBooks = new Batch("TravelBooks");
        travelBooks.addBatch(europe);
        travelBooks.addBatch(asia);
        cookingBooks = new Batch("Cooking Books");

        oop = new Batch("OOP");
        oop.addBook(designPatterns);
        oop.addBook(cleanCode);

        fp = new Batch("Functional Programming");
        fp.addBook(ifp);
        fp.addBook(rwfp);

        software = new Batch("Software");
        software.addBatch(oop);
        software.addBatch(fp);



        p1 = new Batch("P1");
        p2 = new Batch("P2 reading");
        p2.addBatch(p1);
        shop = new Shop();
        shop.addBook(pharo, 100);
        shop.addBook(rwfp, 250);
        shop.addBook(ifp, 125);
        shop.addBook(cleanCode, 20);
        shop.addBook(designPatterns, 450);
    }

    @Test
    public void singleBookSummary() {
        /*
        Create an order with a single book and make sure the output is correct.
		 */

        Calendar orderDate = Calendar.getInstance();
        orderDate.set(2017, 04, 16);
        BookOrder order = new BookOrder(
                "Anonymous", // client name
                orderDate // order date
        );
        order.addBook(patterns, 2);

        String expected = "Date: 16 May, 2017\n" +
                "Client: Anonymous\n" +
                "Number of books: 2\n" +
                "Final Price: 80.4 CHF\n" +
                "\n" +
                "Books\n" +
                "1.\tTitle: Design patterns : elements of reusable object-oriented software\n" +
                "\tAuthor(s): Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides\n" +
                "\tPrice: 40.2 CHF\n" +
                "\tQuantity: 2";

        assertEquals(expected, order.summary());
    }

    @Test
    public void batchOrder() {
        /*
        Create an order with an empty batch and another batch containing the patterns book.
		 */

        Calendar orderDate = Calendar.getInstance();
        orderDate.set(2017, 04, 16);
        BookOrder order = new BookOrder(
                "Anonymous", // client name
                orderDate // order date
        );

        // create batches through the order, so there is no need to add the batch to the
        // order in an extra step
        Batch emptyBatch = order.newBatch("Empty batch");
        Batch patternsBatch = order.newBatch("Design Patterns");

        patternsBatch.addBook(patterns, 1);

        String expected = "Date: 16 May, 2017\n" +
                "Client: Anonymous\n" +
                "Number of books: 1\n" +
                "Total Price: 40.2 CHF\n" +
                "Discount: 0 CHF\n" +
                "Final Price: 40.2 CHF\n" +
                "\n" +
                "Batch 'Empty batch'\n" +
                "\n" +
                "Batch 'Design Patterns'\n" +
                "\n" +
                "\tTitle: Design patterns : elements of reusable object-oriented software\n" +
                "\tAuthor(s): Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides\n" +
                "\tPublisher:  Addison Wesley\n" +
                "\tPublication Date: 31 October 1994\n" +
                "\tNumber of pages: 416\n" +
                "\tCategory: Software Engineering\n" +
                "\tPrice: 40.2 CHF\n" +
                "\tQuantity: 1";

        assertEquals(expected, order.details());
    }
}

/*
The raw representation of the two examples are below.

Note that you do not have to print the orders exactly (e.g., exact same whitespace) as shown below,
but all the information should be present and nested batches should be indented.
 */

/*
Date: 16 May, 2017
Client: Anonymous
Number of books: 2
Final Price: 80.4 CHF


Books

1.  Title: Design patterns : elements of reusable object-oriented software
    Author(s): Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides
    Price: 40.2 CHF
	Quantity: 2
 */


/*
Date: 16 May, 2017
Client: Anonymous
Number of books: 1
Total Price: 40.2 CHF
Discount: 0 CHF
Final Price: 40.2 CHF

Batch 'Empty batch'

Batch 'Design Patterns'

	Title: Design patterns : elements of reusable object-oriented software
    Author(s): Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides
    Publisher:  Addison Wesley
    Publication Date: 31 October 1994
    Number of pages: 416
    Category: Software Engineering
    Price: 40.2 CHF
    Quantity: 1
 */