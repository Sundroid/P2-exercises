package snakes;

public interface ISquare {
	/**
	 * Get the position of the square on the board
	 * @return positive integer
	 */
	public int position();
	/**
	 * Moves the player to another square according to the square movement rules
	 * @param moves The number of squares the player moves
	 * @return Where the player Lands
	 */
	public ISquare moveAndLand(int moves);
	/**
	 * Returns if this square is the special square type FirstSquare there should only be 
	 * one.
	 * @return True if FirstSquare
	 */
	public boolean isFirstSquare();
	/**
	 * Returns if this square is the special square type LastSquare there should only be 
	 * one. 
	 * @return True if FirstSquare
	 */
	public boolean isLastSquare();
	/**
	 * Enters a player on the square so that its now occupied should not be called if 
	 * occupied already except for the first square
	 * @param player Who will be enter on the square
	 */
	public void enter(Player player);
	/**
	 * Removes this player from the square so that its no longer occupied
	 * @param player Who will leave/removed from the square
	 */
	public void leave(Player player);
	/**
	 * Checks if there a player on this square 
	 * @return true if there's one or more players on the square
	 */
	public boolean isOccupied();
	/**
	 * 
	 * Checks if its allowed to land on this square, its allowed if its not occupied and 
	 * not a special square you cant land on.
	 * @return This square if not occupied FirstSquare else (except special Square)
	 */
	public ISquare landHereOrGoHome();
	/**
	 * Returns if a this square is the special square type Wormhole Exit
	 * 
	 * @return True if Wormhole Exit
	 */
	boolean isWormholeExit();
}
