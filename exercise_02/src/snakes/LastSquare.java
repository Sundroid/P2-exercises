package snakes;
// Landing on this square ends the game
// Only thing special about this square is that it knows its the LastSquare
public class LastSquare extends Square {

	public LastSquare(Game game, int position) {
		super(game, position);
	}

	@Override
	public boolean isLastSquare() {
		return true;
	}
}
