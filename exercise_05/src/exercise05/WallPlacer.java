package exercise05;

import java.util.ArrayList;
import java.util.List;

/**
 * This class places walls in given positions if they are valid
 *
 * @author Tobias
 */
public class WallPlacer {


    private Game game;
	private Player player;

    public WallPlacer(Game game, Player player, int x1, int y1, int x2, int y2) throws PlayerException {
        this.game = game;
        this.player = player;
        placeWalls(x1, y1, x2, y2);
    }
     /**
     * Check for all player if they can still reach the end or not
     *
     * @return false if one player can not reach his goal side
     */
    public boolean checkIfvalidPathsExists() {
        for (Player player : game.getPlayers()) {
            if (!canReachGoal(player)) {
                return false;
            }

        }
        return true;
    }


    /**
     * Checks if the player can reach his goal side
     *
     * @param player which is checked if he can reached the end
     * @return false when goal is blocked even temporarily
     */
    private boolean canReachGoal(Player player) {
        int x = player.getPositionX();
        int y = player.getPositionY();
        List<Tile> tilesVisited = new ArrayList<Tile>();
        Tile currentTile = game.getTile(x, y);
        int i = 0;
        tilesVisited.add(currentTile);

        while (i < tilesVisited.size()) {
            if (game.IsOver(player, currentTile.getX(), currentTile.getY())) {
                return true;
            }

            currentTile = tilesVisited.get(i);
            if (currentTile.canBeEntered() || i == 0) {

				for (Tile tile : currentTile.adjacentTiles()) {
					if (!tilesVisited.contains(tile)) {
						tilesVisited.add(tile);
					}
				}

            }
            i++;
        }

        return false;
    }

    /**
     * Placing two wall tiles on tiles that where previously empty, Player must
     * have more than 0 walls left to place, also tiles must be right next to
     * each other.
     *
     * @param x1 xPosition of the first tile
     * @param y1 yPosition of the first tile
     * @param x2 xPosition of the second tile
     * @param y2 yPosition of the second tile
     * @throws PlayerException when one of the above conditions is not met
     */
    public void placeWalls(int x1, int y1, int x2, int y2) throws PlayerException {
        if (!(player.getRemainingWalls() > 0)) {
            throw new PlayerException("You have already placed all of your Walls");
        }
        if (!(game.isValidPosition(x1, y1) && game.isValidPosition(x2, y2))) {
            throw new PlayerException("One or both of the specified tiles are not valid");
        }
        if (!(game.getTile(x1, y1).isAdjacentTile(game.getTile(x2, y2)))) {
            throw new PlayerException("The two tiles are not adjacent to each other");
        }
        game.setWall(x1, y1);
        game.setWall(x2, y2);
        if (!checkIfvalidPathsExists()) {

            game.unsetWall(x1, y1);
            game.unsetWall(x2, y2);
            throw new PlayerException("It was no longer possible to reach the End for one or more Players");
        }
        player.reduceRemainingWalls();

    }


}