package snakes.squares;

import org.junit.Test;
import snakes.Game;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by julian on 3/21/17.
 */
public class WormholeEntranceSquareTest extends StandardSquareTest {
    @Test
    public void testMoveAndLandAndLandHereOrGoHome() {
        Game game = mock(Game.class);
        Square testSquare;
        Square start, stop;
        when(game.isValidPosition(anyInt())).thenReturn(true);
        testSquare = new WormholeEntranceSquare(game, 1);
        start = mock(Square.class);
        stop = mock(Square.class);

        when(game.findSquare(1, 2)).thenReturn(start);
        when(start.landHereOrGoHome()).thenReturn(stop);

        Square destination = testSquare.moveAndLand(2);
        assertEquals(stop, destination);
    }

    @Test
    public void testNoExits(){
        Game game = mock(Game.class);
        Square testSquare;
        Square start, stop;
        when(game.isValidPosition(anyInt())).thenReturn(true);
        when(game.wormholeExits()).thenReturn(new LinkedList<Square>());
        testSquare = new WormholeEntranceSquare(game, 1);
        start = mock(Square.class);
        stop = mock(Square.class);

        when(game.findSquare(1, 2)).thenReturn(testSquare);
        when(start.landHereOrGoHome()).thenReturn(stop);

        Square destination = testSquare.moveAndLand(2);
        assertEquals(testSquare, destination);
    }
}
