package exercise05;

/**
 * Thrown when the player could not be created correctly
 */
public class PlayerException extends Exception {

    public PlayerException(String string) {
        super(string);
    }

}
