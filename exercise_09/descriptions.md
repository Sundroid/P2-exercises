
We changed/created constructors using ServiceLocator for Game/UserInteractor/Driver
For changes every one of these classes got a new constructor and we created interfaces for game
for PrintStream we didn't considering that this was not one of our classes. Also it was necessary
to add ServiceLocator and DefaultServiceLocator and TestServiceLocator.

For all problems adding the fact that we now use an Interface for Game called IGame does not have
a big impact on our implementation.

Dependency Injection was already used in our solution we handed in last time.
So their were no changes necessary except that Driver now has a constructor with Dependency Injection

For SilentTesting we only added one test for Driver which together with UserInteractor are the only classes that
printed something with the new Class NullPrintStream we were able to have SilentTesting for that test

I think the Dependency Injection works better
the problem with this ServiceLocator is the fact 
that in one method call is not easy to see how it's called so what the parameters are.
Also the ServiceLocator and its subclasses seam to make the design more complicated.