/**
 * Created by julian on 5/29/17.
 */

import exercise_10.Batch;
import exercise_10.Book;
import exercise_10.BookOrder;
import exercise_10.Shop;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static exercise_10.Shop.shop;
import static org.junit.Assert.assertEquals;

public class Tests {

    public Book designPatterns, cleanCode, rwfp, ifp, pharo;
    public Batch p2, p1, oop, fp, software, cookingBooks, travelBooks, europe, asia;

    @Before
    public void initialize() {
        Calendar cDesignPatterns = Calendar.getInstance();
        cDesignPatterns.set(1994, Calendar.OCTOBER, 31);
        designPatterns = new Book("Design patterns : elements of reusable object-oriented software", "Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides", "Addison Wesley", "Software Engineering", 416, 40.2, cDesignPatterns);

        Calendar cCleanCode = Calendar.getInstance();
        cCleanCode.set(2008, Calendar.AUGUST, 01);
        cleanCode = new Book("Clean Code: A Handbook of Agile Software Craftsmanship", "Robert C. Martin", "Prentice Hall", "Software Engineering", 464, 50, cCleanCode);

        Calendar cIfp = Calendar.getInstance();
        cIfp.set(1998, Calendar.APRIL, 29);
        ifp = new Book("Introduction Functional Programming", "Richard Bird, Philip Wadler", "Prentice Hall", "Software Engineering", 448, 112.74, cIfp);

        Calendar cRwfp = Calendar.getInstance();
        cRwfp.set(2010, Calendar.JANUARY, 25);
        rwfp = new Book("Real-World Functional Programming", "Tomas Petricek, Jon Skeet", "Manning Publications", "Software Engineering", 500, 35.16, cRwfp);


        // Null object use
        Calendar generic = Calendar.getInstance();
        generic.set(1970, Calendar.JANUARY, 01);

        pharo = new Book("Pharo by Example", "Hans", "Fritz", "Software Engineering", 10, 19, generic);


        europe = new Batch("Europe");
        asia = new Batch("Asia");
        travelBooks = new Batch("TravelBooks");
        travelBooks.addBatch(europe);
        travelBooks.addBatch(asia);
        cookingBooks = new Batch("Cooking Books");

        oop = new Batch("OOP");
        oop.addBook(designPatterns);
        oop.addBook(cleanCode);

        fp = new Batch("Functional Programming");
        fp.addBook(ifp);
        fp.addBook(rwfp);

        software = new Batch("Software");
        software.addBatch(oop);
        software.addBatch(fp);


        p1 = new Batch("P1");
        p2 = new Batch("P2 reading");
        p2.addBatch(p1);
        shop = new Shop();
        shop.addBook(pharo, 100);
        shop.addBook(rwfp, 250);
        shop.addBook(ifp, 125);
        shop.addBook(cleanCode, 20);
        shop.addBook(designPatterns, 450);
    }

    @Test
    public void addBookToShop() {
        Book a = new Book("a", "Hans Meier", "Fritz", "Software Engineering", 123, 32.1, Calendar.getInstance());

        shop.addBook(a);
        assertEquals((long) 1, (long) shop.get(a));
    }

    @Test
    public void createOrderAndRemoveBooksFromShop() {
        BookOrder order = new BookOrder("anonymous", Calendar.getInstance());
        shop.addBook(designPatterns, 12);
        order.addBook(designPatterns, 10);
        assert (order != null);
        assertEquals((long) 452, (long) shop.get(designPatterns));
    }

    @Test
    public void checkEverything() {
        BookOrder order = new BookOrder("anonymous", Calendar.getInstance());
        order.addBatch(software);
        System.out.println(order.details());
    }
}
