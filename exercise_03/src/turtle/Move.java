package turtle;

import java.util.List;

import javafx.util.Pair;
import javafx.util.converter.NumberStringConverter;

/**
 * Stores information on the moves
 * And fills the fields in between the start and end position
 * 
 * @author Tobias
 *
 */
public class Move {
	
	//@Silas: CAUTION: this is a violation of the encapsulation principle. Always make your fields private. Don't let anyone else play with your toys.
	int xMove; 
	int yMove;
	boolean isJump=false;
	/**
	 * Creates a new instance of Move
	 * 
	 * @param x Amount of steps in x direction (Positive x=east)
	 * @param y Amount of steps in y direction (Positive y=south);
	 */


	/**
	 * Creates a move out of the Lists of Strings and Integers 
	 * @param words Should only consists of (North South West East) or is jump or goto
	 * @param numbers Should be one or two items long
	 */
	public Move(List<String> words, List<Integer> numbers) {
		assert !words.isEmpty();
		assert numbers.size()==1||numbers.size()==2;
		
			final Pair<Integer, Integer> v = findDirection(words);
			xMove = numbers.get(0) * v.getKey();
			yMove = numbers.get(0) * v.getValue();
		if(numbers.size()==2){
			gotoJump(words,numbers);
			
			
		}

	}
	/**
	 * 
	 * Called when their is more than one number
	 * @param words should only be one element long and the word should be goto and jump
	 * @param numbers numbers must be 2 elements long
	 */
	void gotoJump(List<String> words,List<Integer> numbers){
		assert(words.size()==1);
		assert(words.get(0)=="goto"||words.get(0)=="jump");
		assert numbers.size()==2;
		
	
		if(words.get(0).equals("jump")){
			isJump=true;
		}
		xMove=numbers.get(0);
		yMove=numbers.get(1);
		
	}
	/**
	 * Creates the Direction where words points to 
	 * @param words (south west east north) will each give a increase in that direction
	 * @return A Pair of ints
	 */
	static Pair<Integer, Integer> findDirection(List<String> words) {
		int x = 0;
		int y = 0;
		for (int i = 0; i < words.size(); i++) {
			switch (words.get(i)) {
			case "north":
				y--;
				break;
			case "south":
				y++;
				break;
			case "east":
				x++;
				break;
			case "west":
				x--;
				break;
			}

		}
		return new Pair<>(x, y);

	}

	/**
	 * Does the move according to the moves rules ending in x+xMove and y+yMove
	 * @param x starting position in x Direction
	 * @param y starting position in y Direction
	 * @param maker The boardmaker that does the move
	 */
	public void makeMove(int x, int y, BoardMaker maker) {
		if(!isJump)drawLine(x, y, maker);
		else jump(x,y,maker);
	}
	/**
	 * Directly goes to end Position without drawing the line
	 * @param x starting position in x Direction
	 * @param y starting position in y Direction
	 * @param maker The boardmaker that does the move
	 */
	private void jump(int x, int y, BoardMaker maker) {
		maker.fill(xMove,yMove);
	}
	/**
	 * Draws the to line x+xMove,y+yMove 
	 * Implements Bresenham Algorithm according to Wikipedia
	 * @param x starting position in x Direction
	 * @param y starting position in y Direction
	 * @param maker The boardmaker that does the move
	 * 
	 * --@Silas: nice algorithm, but isn't it a bit too complicated for just drawing
	 * straight and diagonal lines...?  Also, please be careful about your variable names,
	 * this is barely readable. And maybe avoid German words in your code, because you'll
	 * do a lot of your future coding in international teams, where not everyone speaks German.
	 */
	public void drawLine(int x, int y, BoardMaker maker) {
		int el, es, pdx, pdy, ddx, ddy, ady, adx;
		adx = Math.abs(xMove);
		ady = Math.abs(yMove);
		int xD = 1;
		int yD = 1;

		if (xMove < 0) {
			xD = -1;
		}
		if (yMove < 0) {
			yD = -1;
		}
		if (adx > ady) {
			pdx = xD;
			pdy = 0;
			ddx = xD;
			ddy = yD;
			es = ady;
			el = adx;

		} else {
			pdx = 0;
			pdy = yD;
			ddx = xD;
			ddy = yD;
			es = adx;
			el = ady;
		}
		maker.fill(x, y);
		int fehler = el / 2;
		for (int i = 0; i < el; i++) {
			fehler -= es;
			if (fehler < 0) {
				fehler += el;
				x += ddx;
				y = y + ddy;

			} else {
				x += pdx;
				y += pdy;
			}
			maker.fill(x, y);
		}

	}

	/**
	 * Checks if an object is the same as another
	 * @param obj some object
	 * @return true if object is the same as the current instance
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Move other = (Move) obj;
		if (xMove != other.xMove)
			return false;
		if (yMove != other.yMove)
			return false;
		return true;
	}

	/**
	 * Overrides the default toString() method.
	 * @return the currents object as string
	 */

	@Override
	public String toString() {
		return "Move [xMove=" + xMove + ", yMove=" + yMove + "]";
	}

}
