### The Start Screen
The player has to press enter or click start.
The UI should be quite self-explanatory. 
![alt text](prototype/Start_Screen.jpg "Start Screen")

### The main screen

initial state of the Board

![alt text](prototype/Initial_State.jpg "Initial State of the Board")

The first move

![alt text](prototype/First_Move.jpg "First Move of the orange Player")

Another move, later in the Game

![alt text](prototype/Another_Move.jpg "Another Move later in the Game")

A freshly placed wall (partially covered by the box above the doughnut)

![alt text](prototype/Wall_Placement.jpg "Main Screen with one more move made")

### The end screen
![alt text](prototype/End_Screen.jpg "End Screen")