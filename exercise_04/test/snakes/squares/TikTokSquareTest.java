package snakes.squares;

import org.junit.Before;
import org.junit.Test;
import snakes.Game;
import snakes.Player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by julian on 3/21/17.
 */
public class TikTokSquareTest extends StandardSquareTest {

    @Test
    public void LandHereOrGoHomeNotOccupied() {
        Square testSquare;
        testSquare = new TikTokSquare(game, 1, 2, 3);
        Square stop = mock(Square.class);
        when(game.getSquare(2)).thenReturn(stop);
        when(game.getSquare(3)).thenReturn(stop);
        when(stop.landHereOrGoHome()).thenReturn(stop);
        Square destination = testSquare.landHereOrGoHome();
        assertEquals(stop, destination);
    }

    @Test
    public void LandHereOrGoHomeOccupied() {
        Square testSquare;
        testSquare = new TikTokSquare(game, 1, 2, 3);
        Square firstSquare=mock(Square.class);
        Square stop = mock(Square.class);
        when(game.getSquare(2)).thenReturn(stop);
        when(game.getSquare(3)).thenReturn(stop);
        when(stop.landHereOrGoHome()).thenReturn(firstSquare);
        //when(game.firstSquare()).thenReturn(firstSquare);
        Square destination = testSquare.landHereOrGoHome();
        assertEquals(firstSquare, destination);
    }
}
