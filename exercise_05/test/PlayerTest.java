import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import exercise05.Game;
import exercise05.Player;
import exercise05.PlayerException;
import exercise05.Tile;

public class PlayerTest {

    protected String[] playerString;
    protected Game game;

    @Before
    public void setUp() {
        game = mock(Game.class);
        when(game.isValidPosition(anyInt(), anyInt())).thenReturn(true);
        playerString = new String[5];
        playerString[0] = "John Doe";
        playerString[1] = "J";
        playerString[2] = "2";
        playerString[3] = "3";
        playerString[4] = "R";

    }

    @Test
    public void wallPlacement() throws PlayerException {
        Player player = new Player(playerString, game);
        assertEquals(player.getRemainingWalls(), 5);
        Tile tile = mock(Tile.class);
        when(game.getTile(anyInt(), anyInt())).thenReturn(tile);
        when(tile.isAdjacentTile(any(Tile.class))).thenReturn(true);
        when(game.checkIfvalidPathsExists()).thenReturn(true);
        player.placeWalls(2, 1, 2, 2);
        assertEquals(player.getRemainingWalls(), 4);

    }

    @Test
    public void getName() throws PlayerException {
        Player player = new Player(playerString, game);
        assertEquals("John Doe", player.getName());

    }

    @Test
    public void getPositionX() throws PlayerException {
        Player player = new Player(playerString, game);
        assertEquals(2, player.getPositionX());

    }

    @Test
    public void getPositionY() throws PlayerException {
        Player player = new Player(playerString, game);
        assertEquals(3, player.getPositionY());

    }

    @Test
    public void getSign() throws PlayerException {
        Player player = new Player(playerString, game);
        assertEquals("J", player.getSign());

    }

    @Test
    public void getGoalSide() throws PlayerException {
        Player player = new Player(playerString, game);
        assertEquals("R", player.getGoalSide());

    }

    @Test
    public void moveTestX() throws PlayerException {
        Player player = new Player(playerString, game);
        int xBefore = player.getPositionX();
        int yBefore = player.getPositionY();
        Tile mockTile = mock(Tile.class);
        when(game.getTile(xBefore, yBefore)).thenReturn(mockTile);
        when(game.getTile(xBefore + 1, yBefore)).thenReturn(mockTile);
        player.move(1, 0);
        assertEquals(xBefore + 1, player.getPositionX());
        assertEquals(yBefore, player.getPositionY());
        
    }

    @Test
    public void moveTestY() throws PlayerException {
        Player player = new Player(playerString, game);
        int xBefore = player.getPositionX();
        int yBefore = player.getPositionY();
        Tile mockTile = mock(Tile.class);
        when(game.getTile(xBefore, yBefore)).thenReturn(mockTile);
        when(game.getTile(xBefore, yBefore + 1)).thenReturn(mockTile);
        player.move(0, 1);
        assertEquals(xBefore, player.getPositionX());
        assertEquals(yBefore + 1, player.getPositionY());

    }

    @Test(expected = AssertionError.class)
    public void moveTestfalseArguments() throws PlayerException {
        Player player = new Player(playerString, game);
        player.move(1, 1);
    }

    @Test(expected = AssertionError.class)
    public void moveTestfalseArguments2() throws PlayerException {
        Player player = new Player(playerString, game);
        player.move(-1, 1);
    }

    @Test(expected = AssertionError.class)
    public void moveTestfalseArguments3() throws PlayerException {
        Player player = new Player(playerString, game);
        player.move(2, 0);
    }

    @Test(expected = AssertionError.class)
    public void moveTestfalseArguments4() throws PlayerException {
        Player player = new Player(playerString, game);
        player.move(0, 0);
    }
}
