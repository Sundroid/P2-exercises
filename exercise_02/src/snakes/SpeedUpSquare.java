package snakes;
/*
 * Landing on here is like landing on a regular square
 * If a player is on this Square, every number rolled for movement is doubled
 */
public class SpeedUpSquare extends Square {
	public SpeedUpSquare(Game game, int position) {
		super(game, position);
	}
	@Override
	public ISquare moveAndLand(int moves){
		return super.moveAndLand((moves)*2);
	}

	@Override
	public String squareLabel() {
		return String.format("%d (SpeedUp)", position);
	}
}