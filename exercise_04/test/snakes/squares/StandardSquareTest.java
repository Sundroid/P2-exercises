package snakes.squares;

import snakes.Game;
import snakes.Player;
import snakes.squares.Square;
import snakes.squares.StandardSquare;
import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class StandardSquareTest {
	protected Game game;
	@Before
	public void setUp(){
		game = mock(Game.class);
		when(game.isValidPosition(anyInt())).thenReturn(true);
	}
	@Test
	public void testMoveAndLandOnly() {
		
		Square testSquare;
		Square start, stop;
		
		testSquare = new StandardSquare(game, 1);
		start = mock(Square.class);
		stop = mock(Square.class);

		when(game.findSquare(1, 2)).thenReturn(start);
		when(start.landHereOrGoHome()).thenReturn(stop);

		Square destination = testSquare.moveAndLand(2);
		assertEquals(stop, destination);
	}
	@Test
	public void LandHereOrGoHomeNotOccupied() {
		Game game = mock(Game.class);
		when(game.isValidPosition(anyInt())).thenReturn(true);
		Square testSquare;
		testSquare=new StandardSquare(game,1);
		Square firstSquare=mock(Square.class);
		when(game.firstSquare()).thenReturn(firstSquare);
		Square destination = testSquare.landHereOrGoHome();
		assertNotEquals(firstSquare,destination);
	}
	
	@Test
	public void LandHereOrGoHomeOccupied() {
		Game game = mock(Game.class);
		when(game.isValidPosition(anyInt())).thenReturn(true);
		Square testSquare;
		testSquare=new StandardSquare(game,1);
		Square firstSquare=mock(Square.class);
		when(game.firstSquare()).thenReturn(firstSquare);
		Player player=mock(Player.class);
		testSquare.enter(player);
		Square destination = testSquare.landHereOrGoHome();
		assertEquals(firstSquare,destination);
	}
	@Test
	public void occupied(){
		
		Square testSquare=new StandardSquare(game,1);
		Player player=mock(Player.class);
		testSquare.enter(player);
		assertTrue(testSquare.isOccupied());
	}
	
	@Test
	public void leave(){
		
		Square testSquare=new StandardSquare(game,1);
		Player player=mock(Player.class);
		testSquare.enter(player);
		assertTrue(testSquare.isOccupied());
		testSquare.leave(player);
		assertFalse(testSquare.isOccupied());
	}
	@Test
	public void isFirstSquareFalse(){
		
		Square testSquare=new StandardSquare(game,1);
		assertFalse(testSquare.isFirstSquare());
	}
	@Test
	public void isLastSquareFalse(){
		
		Square testSquare=new StandardSquare(game,1);
		assertFalse(testSquare.isLastSquare());
	}
	@Test
	public void isWormHoleExitFalse(){
		
		Square testSquare=new StandardSquare(game,1);
		assertFalse(testSquare.isWormholeExit());
	}
}
