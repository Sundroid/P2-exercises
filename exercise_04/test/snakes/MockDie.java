package snakes;

/**
 * Created by julian on 3/21/17.
 */
public class MockDie implements IDie {
    public int roll (){
        return (1+(int)Math.random()*faces);
    }
}

//@Silas: the idea of a mock die is to eliminate the element of chance; hence, a MockDie#roll() should have a deterministic return value.