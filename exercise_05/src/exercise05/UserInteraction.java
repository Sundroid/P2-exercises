package exercise05;

import java.io.PrintStream;

/**
 * Handles the User Interaction
 *
 * @author Tobias
 */
public class UserInteraction {
    PrintStream out;

    public UserInteraction(PrintStream out) {
        this.out = out;
    }
    public UserInteraction(){
    	this.out=ServiceLocator.instance().getOutput();
    }
    public void interact(Player currentPlayer, Game game) {

        while (true) {
            out.println("It's " + currentPlayer.getName() + "'s turn: ");
            try {
                new Command(currentPlayer, currentPlayer.getInput(), game);
                break;
            } catch (Exception e) {
                out.println("Your Move is not valid");
                out.println("Because of: " + e.getMessage());
            }
        }
        out.println(Renderer.draw(game));

    }


}


