import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;

import org.junit.Before;
import org.junit.Test;

import exercise05.Driver;
import exercise05.IGame;
import exercise05.Parser;
import exercise05.PlayerException;
import exercise05.ServiceLocator;
import exercise05.TestServiceLocator;

public class DriverTest {
	IGame game;
	@Before 
	public void setUp() throws IOException, PlayerException{
		TestServiceLocator locator = new TestServiceLocator();
		locator.setInput(createMoveScript());
		BufferedReader br = new BufferedReader(new FileReader("games/DriverTest.txt"));
	    Parser parse = new Parser();
	    BufferedReader moveScript = createMoveScript();
	    game = parse.parse(br, moveScript);
		locator.setGame(game);
		ServiceLocator.setLocator(locator);
	}
	
	@Test
	public void test() throws IOException {
		Driver driver=new Driver();
		driver.play();
		assertTrue(game.IsOver());
		assertEquals(game.getCurrentPlayer().getName(),"Kris Kristofferson");
		assertEquals(3,game.getCurrentPlayer().getPositionX());
		assertEquals(1,game.getCurrentPlayer().getPositionY());
		assertEquals(game.getTileContent(1, 14),"#");
		assertEquals(game.getTileContent(1, 15),"#");
		game.nextPlayer();
		assertEquals(game.getCurrentPlayer().getName(),"Janis Joplin");
		assertEquals(1,game.getCurrentPlayer().getPositionX());
		assertEquals(3,game.getCurrentPlayer().getPositionY());
	}
	private BufferedReader createMoveScript() {
	        StringBuilder moves = new StringBuilder();
	        moves.append("R\n");
	        moves.append("1 14 1 15\n");
	        moves.append("R\n");
	        return new BufferedReader(new StringReader(moves.toString()));
	}
}
