package snakes.squares;

import org.junit.Test;
import snakes.Game;
import snakes.Player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static org.mockito.Mockito.mock;

/**
 * Created by julian on 3/21/17.
 */
public class WormholeExitSquareTest extends StandardSquareTest {
    @Test
    public void isExitSquareTest(){
        Game game = mock(Game.class);
        when(game.isValidPosition(anyInt())).thenReturn(true);
        Square testSquare = new WormholeExitSquare(game, 1);
        assertTrue(testSquare.isWormholeExit());
    }

    @Test
    public void LandHereOrGoHomeNotOccupied() {
        Game game = mock(Game.class);
        when(game.isValidPosition(anyInt())).thenReturn(true);
        Square testSquare;
        testSquare=new StandardSquare(game,1);
        Square firstSquare=mock(Square.class);
        when(game.firstSquare()).thenReturn(firstSquare);
        Square destination = testSquare.landHereOrGoHome();
        assertNotEquals(firstSquare,destination);
    }

    @Test
    public void LandHereOrGoHomeOccupied() {
        Game game = mock(Game.class);
        when(game.isValidPosition(anyInt())).thenReturn(true);
        Square testSquare;
        testSquare=new StandardSquare(game,1);
        Square firstSquare=mock(Square.class);
        when(game.firstSquare()).thenReturn(firstSquare);
        Player player=mock(Player.class);
        testSquare.enter(player);
        Square destination = testSquare.landHereOrGoHome();
        assertEquals(firstSquare,destination);
    }
}
