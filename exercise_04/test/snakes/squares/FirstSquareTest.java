package snakes.squares;

import org.junit.Before;
import org.junit.Test;
import snakes.Game;
import snakes.Player;
import snakes.squares.Square;
import snakes.squares.FirstSquare;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by julian on 3/21/17.
 */
public class FirstSquareTest extends StandardSquareTest {

    @Test
    public void landHereOrGoHome(){
        Square testSquare = new FirstSquare(game,  1);
        Player player = mock(Player.class);
        Player player1 = mock(Player.class);
        testSquare.enter(player);
        testSquare.enter(player1);
        assertEquals(player.position(), player1.position());
    }

    @Test
    public void occupied(){
        Square testSquare=new FirstSquare(game,1);
        Player player=mock(Player.class);
        testSquare.enter(player);
        assertTrue(testSquare.isOccupied());
    }

    @Test
    public void leave(){
        Square testSquare=new FirstSquare(game,1);
        Player player=mock(Player.class);
        testSquare.enter(player);
        assertTrue(testSquare.isOccupied());
        testSquare.leave(player);
        assertFalse(testSquare.isOccupied());
    }
    @Test
    public void isFirstSquare() {
        Game game = mock(Game.class);
        when(game.isValidPosition(anyInt())).thenReturn(true);
        Square testsquare=new FirstSquare(game,1);
        assertTrue(testsquare.isFirstSquare());
    }
}