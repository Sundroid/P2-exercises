package exercise05;

/**
 * Renders a {@link Game} object. Abstract class because no instances need to be
 * created everything is done statically
 */
public abstract class Renderer {
    /**
     * Creates a String from your game which is filed with the content of the
     * games tiles
     *
     * @param game Draws this game
     * @return String which has {@link game.width} characters than \n and this
     * {@link game.height} times
     */
    public static String draw(IGame game) {
        StringBuilder s = new StringBuilder();
        int width = game.getWidth();
        int height = game.getHeight();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {

                s.append(game.getTileContent(j, i));

            }
            s.append("\n");

        }
        return s.toString();

    }

}