package exercise05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class DefaultServiceLocator extends ServiceLocator {
	@Override
	public PrintStream getOutput() {
		return System.out;
	}

	@Override
	public UserInteraction getUserInteractor() {
		return(new UserInteraction(System.out));
	}

	@Override
	public IGame getIGame() {
		try {
			return Driver.createIGameFromFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public BufferedReader getInput() {
		// TODO Auto-generated method stub
		return new BufferedReader(new InputStreamReader(System.in));
	}
	
}
