/**
 * Filters file names using command-line wildcards.
 *
 * '*' matches any number of character.
 * '?' matches exactly one character.
 *
 * Examples:
 * '*.md' matches all files with the markdown extension.
 * 'exercise_??.md' matches, for example, 'exercise_01.md'.
 * 
 * @author You!
 *
 */
public class FilePattern {
	/**
	 * Creates a new instance of the FilePattern class that filters
	 * file names based on the given pattern.
	 * 
	 * @param pattern the pattern used to filter file names.
	 * @see FilePattern
	 */
	private String pattern;
	public FilePattern(String pattern) {
		this.pattern=pattern;
	}

	/**
	 * Returns whether the given filename matches this pattern.
	 * @param filename
	 * @return true if filename matches the pattern
	 */
	public boolean matches(String filename) {
		int x=0;
		int y=0;
		
		return matchesPart(filename,x,y);
		
	}
	private boolean matchesPart(String filename,int startPattern, int startFilename){
		
		while(startPattern<pattern.length()&&startFilename<filename.length()){
		
			//@Silas: consider using a switch-structure here; switch(pattern.charAt(startPattern)){ ... }
			if (filename.charAt(startFilename)==pattern.charAt(startPattern)){
				startPattern++;
				startFilename++;
			}
			
			else if (pattern.charAt(startPattern)=='?'){
				startPattern++;
				startFilename++;
				
			}
			
			else if (pattern.charAt(startPattern)=='*'){
				if(matchesPart(filename,startPattern+1,startFilename)){return true;}
				startFilename++;
			}
			else {return false;}
			
		}
		if(startPattern==pattern.length()-1&&pattern.charAt(startPattern)=='*'){
			return true;
			
		}
		if(startFilename!=filename.length())
			return false;
		
		if(startPattern!=pattern.length())
			return false;
		
		return true;
	}
    
}
