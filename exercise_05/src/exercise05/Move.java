package exercise05;

/**
 * A simple one tile move moving a player one place
 * Check with the game if this is allowed
 *
 * @author Tobias
 */
public class Move {

    private Player player;
    private String move;

    public Move(String move, Player player) throws PlayerException {
        this.player = player;
        this.move = move;
        execute(move);
    }

    private void execute(String move) throws PlayerException {
        switch (move) {
            case "D":
                player.move(0, 1);
                return;
            case "L":
                player.move(-1, 0);
                return;
            case "R":
                player.move(1, 0);
                return;
            case "U":
                player.move(0, -1);
                return;
        }

    }

}
