package snakes;

import org.junit.Before;
import org.junit.Test;
import snakes.squares.FirstSquare;
import snakes.squares.Square;
import snakes.Player;
import snakes.Game;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by julian on 3/21/17.
 */
public class GamePlayTestMockito {
    protected Queue<Player> players;
    protected Player player1, player2;
    protected Square square;
    protected IDie die;
    protected Game game;

    @Before
    public void setUp() {
        players = new LinkedList<>();
        player1 = mock(Player.class);
        player2 = mock(Player.class);
        square = mock(Square.class);
        die = mock(IDie.class);
        when(die.roll()).thenReturn(1);


        players.add(player1);
        players.add(player2);

        game = new Game(10, players);
    }

    @Test
    public void testGameOver() {
        when(player1.wins()).thenReturn(true);
        game.play(die);
        when(player2.wins()).thenReturn(true);
        assertTrue(game.isOver());
    }

    @Test
    public void testWinner() {
        //when(game.currentPlayer()).thenReturn(player1);
        when(die.roll()).thenReturn(1);
        when(player1.wins()).thenReturn(true);
        game.play(die);
        assertTrue(game.isOver());
        try {
            assertEquals(player1, game.winner());
        } catch (GameNotOverException e) {
        }
    }

    @Test
    public void testFlow() {
        assertNotEquals(game.nextPlayer(), game.currentPlayer());
        assertEquals(players.peek(), game.currentPlayer());
    }
}
