import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import exercise05.Game;
import exercise05.Parser;
import exercise05.Player;
import exercise05.PlayerException;
import exercise05.ServiceLocator;
import exercise05.TestServiceLocator;
import exercise05.UserInteraction;
public class TestScenarioWithServiceLocator {
    protected Game game;

    @Before
    public void scenarioStart() throws IOException, PlayerException {
    	TestServiceLocator locator = new TestServiceLocator();
		locator.setInput(createMoveScript());
    	BufferedReader br = new BufferedReader(new FileReader("games/testGame.txt"));
        Parser parse = new Parser();
        BufferedReader moveScript = createMoveScript();
        game = parse.parse(br, moveScript);
    	locator.setGame(game);
		ServiceLocator.setLocator(locator);
    }

    @Test
    public void makePlayers() throws PlayerException {
        String[] playerString = {"Kris Kristofferson", "K", "1", "1", "R"};
        String[] playerString2 = {"Janis Joplin", "J", "1", "3", "R"};
        String[] playerString3 = {"Third Guy", "T", "3", "15", "U"};
        List<Player> players = new ArrayList<Player>();
        players.add(new Player(playerString, game));
        players.add(new Player(playerString2, game));
        players.add(new Player(playerString3, game));
        assertEquals(game.getPlayers(), players);
    }

    @Test
    public void makeMoves() throws PlayerException, IOException {
    	
        assertEquals(game.getCurrentPlayer().getName(), "Kris Kristofferson");
        assertEquals(game.getCurrentPlayer().getPositionX(), 1);
        assertEquals(game.getCurrentPlayer().getPositionY(), 1);
        game.nextTurn(new UserInteraction());
        assertEquals(game.getPlayers().get(0).getPositionX(), 1);
        assertEquals(game.getPlayers().get(0).getPositionY(), 2);

        assertEquals(game.getCurrentPlayer().getName(), "Janis Joplin");
        assertEquals(game.getCurrentPlayer().getPositionX(), 1);
        assertEquals(game.getCurrentPlayer().getPositionY(), 3);
        assertEquals(game.getTileContent(1, 14), " ");
        assertEquals(game.getTileContent(1, 15), " ");
        game.nextTurn(new UserInteraction());
        assertEquals(game.getTileContent(1, 14), "#");
        assertEquals(game.getTileContent(1, 15), "#");
        assertEquals(game.getPlayers().get(0).getPositionX(), 1);
        assertEquals(game.getPlayers().get(0).getPositionY(), 2);
    }

    private BufferedReader createMoveScript() {
        StringBuilder moves = new StringBuilder();
        moves.append("D\n");
        moves.append("1 14 1 15\n");

        return new BufferedReader(new StringReader(moves.toString()));
    }
}
