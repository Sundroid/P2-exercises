package exercise_10;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Book {
    private Calendar publicationDate;
    private String title;
    private String authors;
    private String publisher;
    private String category;
    private int pages;
    private double price;

    public Book(String title, String authors, String publisher, String category, int pages, double price, Calendar publicationDate) {
        this.title = title;
        this.authors = authors;
        this.publisher = publisher;
        this.category = category;
        this.pages = pages;
        this.price = price;
        this.publicationDate = publicationDate;
    }

    public Calendar getPublicationDate() {
        return publicationDate;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    protected String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    protected double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthors() {
        return authors;
    }

    public int getPages() {
        return pages;
    }


    /**
     * Returns the book as a string
     * @param isSummary pick between a short and a longer representation
     * @return the book as string
     */
    protected String toString(boolean isSummary) {
        StringBuilder book = new StringBuilder();
        book.append("\tTitle: " + this.title + "\n");
        book.append("\tAuthor(s): " + this.authors + "\n");
        if (!isSummary) {
            book.append("\tPublisher: " + this.publisher + "\n");
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMMMM, yyyy");
            book.append("\tPublication Date: " + sdf.format(this.publicationDate.getTime()) + "\n");
            book.append("\tNumber of pages: " + this.pages + "\n");
            book.append("\tCategory: " + this.category + "\n");
        }
        book.append("\tPrice: " + this.price + " CHF");
        return book.toString();
    }
}
