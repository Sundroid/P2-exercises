package snakes;

import org.junit.Test;
import snakes.IDie;
import snakes.squares.FirstSquare;
import snakes.squares.Square;
import snakes.Player;
import snakes.Game;
import snakes.Player;
import snakes.squares.Square;
import snakes.squares.StandardSquare;

import java.util.LinkedList;
import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by julian on 3/21/17.
 */
public class GamePlayTestMockDie {
    @Test
    public void testGameOver() {
        Queue<Player> players = new LinkedList<>();
        Player playerA = mock(Player.class);
        Player playerB = mock(Player.class);
        players.add(playerA);
        players.add(playerB);
        when(playerB.wins()).thenReturn(true);
        when(playerA.wins()).thenReturn(true);
        IDie die = new MockDie();
        Game game = new Game(10, players);
        game.play(die);
        assertTrue(game.isOver());
    }
    //The of the game#play method can be copied from the Mockito version of this test. You just need to replace the mocked Die die with the IDie as seen in Line 35 of this file.
    /*
     * @Silas: yes, that's correct. However, one idea of using a mocked die is to know exactly which player rolls which number in every turn. That way, you know exactly
     * who's on which square, who gets sent home, and ultimately, who wins the game. Since your MockDie does not return a deterministic value when rolled, you can't 
     * test this behavior here.
     */
}
