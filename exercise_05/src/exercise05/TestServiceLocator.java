package exercise05;

import java.io.BufferedReader;
import java.io.PrintStream;
/**
 * For testing purposes
 * All printStreams are NullPrintStreams 
 * @author Tobias
 *
 */
public class TestServiceLocator extends ServiceLocator {
	IGame game;
	BufferedReader input;
	@Override
	public PrintStream getOutput() {
		return new PrintStream(new NullPrintStream());
	}

	@Override
	public UserInteraction getUserInteractor() {

		return new UserInteraction(new PrintStream(new NullPrintStream()));
	}

	public void setGame(IGame game){
		this.game=game;
	}
	public void setInput(BufferedReader input){
		this.input=input;
	}
	@Override
	public IGame getIGame() {
		
		return game;
	}
	@Override
	public BufferedReader getInput(){
	
		return input;
		
		
	}

}
