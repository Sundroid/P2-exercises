package turtle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.util.Pair;

/**
 * Parses a String to a List<Move>. Creates those moves.
 * The syntax of a move is as follows:
 * 	program ::= move*
 *  move ::= word+ number+
 *  word ::= <Character.isAlphabet>+
 *  number ::= [0-9]+
 *  
 * Where + means 1 or more.
 * Move defines what sequences of words and numbers mean.
 * The task of this class is to call the constructor appropriately for each found move.
 * 
 * Note: Additional numbers are ignored.
 * 
 * Conceptually, this is an immutable object.
 * 
 * @author Tobias
 *
 */
public class CommandParser {
	/** current position within program
	 * position is always between 0 and the length of the string (inclusive: it can point past the end)
	 * this is advanced by each of the "parse*" methods
	 * it is never decreased
	 */
	protected int position;
	/** this will be parsed. position points into this (or past its end) */
	private final String program;

	private void checkInvariance(){
		assert(program != null);
		assert (position >= 0 && position <= program.length());
	}

	/**
	 * 
	 * @param program must not be null and follow the syntax rules,
	 * otherwise undefined behaviour may result.
	 */
	public CommandParser(String program) {
		assert program != null;
		this.program = program;
		position = 0;
		checkInvariance();
	}

	/**
	 * Parses program into the List of moves it represents from the beginning.
	 * Can be called multiple times.
	 *
	 * @return List of moves in the order that their in the string.
	 */
	public List<Move> parse() {
		position = 0;
		checkInvariance();
		
		final List<Move> a = new ArrayList<>();
		while (position < program.length()) {
			final Move m = parseMove();
			a.add(m);
		}
		checkInvariance();
		return a;
	}

	/**
	 * Parses one move that is in the program after position.
	 * 
	 * Precondition: Position
	 * between 0 and s.length
	 *
	 * @return The move that was created
	 */
	protected Move parseMove() {
		checkInvariance();
		final String wordsAndSpaces = parseWordsAndSpaces();
		assert wordsAndSpaces != null;
		assert !wordsAndSpaces.isEmpty();
		final String numbersAndSpaces = parseNumbersAndSpaces();
		assert numbersAndSpaces != null;
		assert !numbersAndSpaces.isEmpty();
		Move m = createMove(wordsAndSpaces, numbersAndSpaces);

		assert m != null;
		checkInvariance();
		return m;
	}

	/**
	 * Creates a move.
	 *
	 * Precondition: word must represent supported direction
	 * 
	 * @param wordsAndSpaces Direction your move goes,case-insensitive
	 * @param numbersAndSpaces amount of steps in this direction or in case of goto the coordinates.
	 * @return Move that represents this command
	 */
	protected Move createMove(String wordsAndSpaces, String numbersAndSpaces) {
		assert wordsAndSpaces != null;
		assert numbersAndSpaces != null;
		assert !wordsAndSpaces.isEmpty();
		assert !numbersAndSpaces.isEmpty();
		
		checkInvariance();

		final String w = wordsAndSpaces.toLowerCase().trim();
		final List<String> words = splitOnSpace(w);
		final List<Integer> numbers = splitNumbers(numbersAndSpaces.trim());

		final Move m = new Move(words, numbers);

		checkInvariance();
		return m;
	}
	

	/**
	 * Sets position to the first character that is not a whitespace
	 */
	protected void advanceWhileSpace() {
		checkInvariance();
		while (position < program.length() && Character.isWhitespace(program.charAt(position))) {
			position++;
		}
		checkInvariance();
	}
	/**
	 * Splits the string on WhiteSpace
	 * @param s should be split 
	 * @return The List will be order the same way the string is
	 *
	 */
	protected static List<String> splitOnSpace(String s) {
		String[] word = s.split("\\s");
		assert word.length > 0;
		return Arrays.asList(word);
		
	}

	/**
	 * Sets position to the first character that is not an alphabetic character
	 * or whitespace
	 */
	protected void advanceWhileCharacterOrSpace() {
		checkInvariance();
		while (position < program.length() && 
				(Character.isAlphabetic(program.charAt(position)) 
						|| Character.isWhitespace(program.charAt(position)))) {
			position++;
		}
		checkInvariance();
	}

	/**
	 * Finds the word that starts at position in the string s
	 *
	 * @return Substring of s which starts at position and ends where
	 *         advanceWhileCharacter stops
	 */
	protected String parseWordsAndSpaces() {
		checkInvariance();
		final int initialPosition = position;
		advanceWhileCharacterOrSpace();
		assert initialPosition < position;
		final String w = program.substring(initialPosition, position);
		assert !w.trim().isEmpty();
		checkInvariance();
		return w;
	}

	/**
	 * Increases Position while this character is a Number or Whitespace
	 */
	protected void advanceWhileNumberOrWhiteSpace() {
		checkInvariance();
		while (position < program.length()
				&& (
						(program.charAt(position) <= '9' && program.charAt(position) >= '0')
				|| Character.isWhitespace(program.charAt(position))))  {
			position++;
		}
		checkInvariance();
	}

	/**
	 * Parses number from beginning of the string Precondition: First character
	 * in string must be a number
	 *
	 * @return int Representation From first character to last character that is
	 *         a number
	 */
	protected String parseNumbersAndSpaces() {
		assert (program.charAt(position) <= '9' && program.charAt(position) >= '0');
		checkInvariance();
		final int initialPosition = position;
		advanceWhileNumberOrWhiteSpace();
		final String n = program.substring(initialPosition, position);
		assert !n.trim().isEmpty();
		checkInvariance();
		return n;
	}

	/** parses ints from a string containing ints and whitespaces
	 *
	 * Precondition: Number(s) and whitespaces
	 * @param s containing at least one int and eventually whitespaces
	 * @return the numbers contained in s as an arraylist
	 */

	protected static List<Integer> splitNumbers(String s) {
		assert s != null;
		final List<Integer> numbers = new ArrayList<>();
		final List<String> words = splitOnSpace(s);
		for (String k : words) {
			final int a = Integer.parseInt(k);
			numbers.add(a);
		}
		assert numbers.size() > 0;
		return numbers;
	}

}
