package exercise05;

/**
 * Decides what type of move a given command by a player is
 *
 * @author Tobias
 */
public class Command {

    private Player player;
    private Game game;

    public Command(Player player, String move, Game game) throws PlayerException {
        this.player = player;
        assert (player != null);
        this.game = game;
        parse(move);

    }

    /**
     * Parse the given String into a move and executes it
     *
     * @param move should be either (D|L|R|U) or x1 y1 x2 y2 (with x1,x2,y1,y2
     *             being integers)
     * @throws PlayerException if the move was either impossible to parse or execute
     */
    private void parse(String move) throws PlayerException {
        if (move.length() == 1) {
            new Move(move, player);
            return;
        }

        int[] array = splitOnSpace(move);
        new WallPlacer(game, player, array[0], array[1], array[2], array[3]);
    }

    /**
     * Helper methods separates on space string should contain 4 numbers
     * seperated by space
     *
     * @param s should be x1 y1 x2 y2
     * @return int[] containing x1,y1,x2,y2 in that order
     * @throws PlayerException when one was not a number or their were not 4 numbers in the
     *                         string
     */
    protected static int[] splitOnSpace(String s) throws PlayerException {
        String[] word = s.split("\\s");
        assert word.length > 0;
        int[] numbers = new int[word.length];
        for (int i = 0; i < word.length; i++) {
            numbers[i] = Integer.parseInt(word[i]);
        }
        if (numbers.length != 4) {
            throw new PlayerException("If you want to place walls it should be x1 y1 x2 y2 and nothing else");
        }

        return numbers;

    }

}
