package snakes.squares;

import org.junit.Before;
import org.junit.Test;
import snakes.Game;
import snakes.Player;
import snakes.squares.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

public class SwapSquareTest extends StandardSquareTest {
    @Test
    @Override
    public void LandHereOrGoHomeOccupied() {
    }

    @Test
    public void swapTest() {
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Square randomSquare=mock(Square.class);
        Square testSquare = new SwapSquare(game,3);
        when(game.isValidPosition(anyInt())).thenReturn(true);
        when(game.nextPlayer()).thenReturn(player1);
        when(game.currentPlayer()).thenReturn(player2);

        when(game.nextPlayer().position()).thenReturn(3);
        when(game.currentPlayer().position()).thenReturn(1);

        when(game.getSquare(3)).thenReturn(randomSquare);
        Square destination = testSquare.landHereOrGoHome();
        assertEquals(randomSquare,destination);
    }
}
