package exercise05;

import java.util.ArrayList;
import java.util.List;

/**
 * Tile of the game checks the rules for entering/stepping on it (can be stepped
 * on when its not a wall and theirs no player on it) Only one player per Tile
 * and no players on Wall tiles allowed
 *
 * @author Tobias
 */
public class Tile {
    private Player player;
    private boolean isWall;
    private int positionX;
    private int positionY;
    private IGame game;
    private String standardContent;

    /**
     * Creates Tile
     *
     * @param isWallTile specified if this tile starts as a wall;
     * @param x          xPosition on the game of this tile
     * @param y          yPosition on the game of this tile
     * @param game       belongs to this game
     */
    public Tile(boolean isWallTile, int x, int y, IGame game) {
        this(isWallTile, x, y, game, " ");
    }

    public Tile(boolean isWallTile, int x, int y, IGame game, String OriginalChar) {
        isWall = isWallTile;
        player = null;
        positionX = x;
        positionY = y;
        this.game = game;
        standardContent = OriginalChar;
    }

    public Tile(boolean isWallTile) {
        this(isWallTile, 0, 0, null);
    }

    public Tile(boolean isWallTile, int x, int y) {
        this(isWallTile, x, y, null);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (isWall ? 1231 : 1237);
        result = prime * result + positionX;
        result = prime * result + positionY;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Tile other = (Tile) obj;
        if (isWall != other.isWall) {
            return false;
        }
        if (positionX != other.positionX) {
            return false;
        }
        if (positionY != other.positionY) {
            return false;
        }
        return true;
    }

    /**
     * Returns the content of this tile
     *
     * @return Content is either {@link}player.sign or " " (if no player) or "#"
     * if wall
     */
    public String getContent() {
        if (player != null) {
            return player.getSign();
        }
        if (isWall == true) {
            return "#";
        }
        return standardContent;
    }

    /**
     * Enter this player on the tile if {@link}isWall==false and
     * {@link}player==null
     *
     * @param _player
     */
    public void enter(Player _player) {
        assert isWall == false;
        assert (this.player == null);
        this.player = _player;
    }

    /**
     * Sets the player on this field to null If this is not a wall Tile and
     * their was a player on here before so {@link}player!=null
     */
    public void leave() {
        assert isWall == false;
        assert (this.player != null);
        this.player = null;
    }

    /**
     * States whether or not a player may be enter here
     *
     * @return true if {@link player}==null and {@link isWall}==false
     */
    public boolean canBeEntered() {
        if (player != null || isWall == true) {
            return false;
        }
        return true;

    }

    public Player getPlayer() {

        return player;
    }

    /**
     * Finds all tiles right next to this tile
     *
     * @return tiles which are on the right left up and down of this tile
     */
    public List<Tile> adjacentTiles() {
        List<Tile> adjacentTiles = new ArrayList<Tile>();
        assert (isWall == false);
        if (game.isValidPosition(positionX + 1, positionY)) {
            adjacentTiles.add(game.getTile(positionX + 1, positionY));
        }

        if (game.isValidPosition(positionX - 1, positionY)) {
            adjacentTiles.add(game.getTile(positionX - 1, positionY));
        }

        if (game.isValidPosition(positionX, positionY - 1)) {
            adjacentTiles.add(game.getTile(positionX, positionY - 1));
        }
        if (game.isValidPosition(positionX, positionY + 1)) {
            adjacentTiles.add(game.getTile(positionX, positionY + 1));
        }
        return adjacentTiles;
    }

    /**
     * Figures out if this tile is right next to this one
     *
     * @param tile you want to check
     * @return true if it is one of the adjacent tile
     */
    public boolean isAdjacentTile(Tile tile) {
        if (adjacentTiles().contains(tile)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Tile [player=" + player + ", isWall=" + isWall + ", positionX=" + positionX + ", positionY=" + positionY
                + "]";
    }

    public int getY() {

        return positionY;
    }

    public int getX() {
        // TODO Auto-generated method stub
        return positionX;
    }

    /**
     * Set a tile to a Wall if it was enter able(no player on it, not a wall)
     */
    public void setToWall() {
        assert (canBeEntered());
        isWall = true;
    }

    /**
     * makes a wall tile empty again Precondition: tile was wall before
     */
    public void unsetToWall() {
        assert (isWall);
        isWall = false;

    }

    /**
     * Overload of isAdjacentTile(Tile tile) Gets tile on position x,y from the
     * game then calls the other method
     */
    public boolean isAdjacentTile(int x, int y) {

        return isAdjacentTile(game.getTile(x, y));
    }

}
