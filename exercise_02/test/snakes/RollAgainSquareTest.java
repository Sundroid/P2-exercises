package snakes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RollAgainSquareTest extends SquareTest {
	@Override
	@Before
	public void newGame() {
		initializeGame(15);
		game.setSquare(7, new RollAgainSquare(game, 7));
	}

	@Test
	public void moveToRollAgainSquare() {
		game.movePlayer(6); // moves Jack
		assertTrue(jack.position()!=7); //Because there a RollAgainSquare here
		assertTrue(jack.position()-7>0); // Position should be between 8 and 13
		assertTrue(jack.position()-14<0);
		game.movePlayer(5); // moves Jill
		assertTrue(jack.position()!=7);
		assertTrue(jack.position()-7>0);
		assertTrue(jack.position()-14<0);
		assertEquals(6, jill.position());
		game.movePlayer(2); // now it's Jill's turn
		assertTrue(jack.position()-9>0); //Position should be between 10 and 15
		assertEquals(6, jill.position());
	}

	
}
