package exercise05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Player is responsible for moving itself and for keeping track where he is
 * Manages the parsing and executing of the moves himself
 *
 * @author Tobias
 */
public class Player {

    private int positionX;
    private int positionY;
    private String goalSide;
    private String sign;
    private String name;
    private final Game game;
    private int remainingWalls;
    private List<Tile> winningTiles;
    private BufferedReader input;

    /**
     * Creates a Player
     *
     * @param player      ("<name>","<sign>","<positionX>","<positionY>","<goalSide>")
     *                    name
     * @param game        must not be null
     * @param inputStream will read commands from this
     * @throws PlayerException
     */
    public Player(String[] player, Game game, BufferedReader inputStream) throws PlayerException {
        this(player, game, inputStream, 5);
    }
    /**
     * Creates a Player from a String[] of length 2 or 5 all other length
     * result in failure.
     * Uses the ServiceLocator.instance() for the BufferedReader
     * @param player      if 5 elements:
     *                    ("<name>","<sign>","<positionX>","<positionY>","<goalSide>")
     *                    with 2:
     *                    ("<sign>","<name>")
     * @param game        must not be null
     * @param walls       specifies how many wall-pairs this player can build
     * @throws PlayerException
     */
    public Player(String[] player, Game game,int walls) throws PlayerException {
        this(player, game, ServiceLocator.instance().getInput(), walls);
    }
    
    /**
     * Creates a Player from a String[] of length 2 or 5 all other length
     * result in failure
     *
     * @param player      if 5 elements:
     *                    ("<name>","<sign>","<positionX>","<positionY>","<goalSide>")
     *                    with 2:
     *                    ("<sign>","<name>")
     * @param game        must not be null
     * @param inputStream will read commands from this
     * @param walls       specifies how many wall-pairs this player can build
     * @throws PlayerException
     */
    public Player(String[] player, Game game, BufferedReader inputStream, int walls) throws PlayerException {
        assert game != null;
        assert player.length == 2 || player.length == 5;
        if (player.length == 2) {
            String temp = player[1];
            player[1] = player[0];
            player[0] = temp;
            winningTiles = new ArrayList<Tile>();
            assert (player[1] != player[1].toLowerCase());
        } else {
            assert player[1].length() == 1;
            assert player[4].length() == 1;
            positionX = Integer.parseInt(player[2]);// - 1;
            positionY = Integer.parseInt(player[3]);// - 1;
            goalSide = player[4];
            winningTiles = game.getSideTiles(goalSide);
        }
        name = player[0];
        sign = player[1];
        input = inputStream;
        this.game = game;
        remainingWalls = walls;
        assert positionX >= 0;
        assert positionY >= 0;
    }
   
    
    
    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public void addWinningTile(Tile tile) {
        winningTiles.add(tile);
    }


    /**
     * Creates a Player
     *
     * @param player ("<name>","<sign>","<positionX>","<positionY>","<goalSide>")
     *               name
     * @param game   must not be null
     * @throws PlayerException
     */
    public Player(String[] player, Game game) throws PlayerException {
        this(player, game, new BufferedReader(new InputStreamReader(System.in)));
    }

    /**
     * Moves this player one step if the move is valid Only one of x and y can
     * be!=0 and both must be either -1,0,1
     *
     * @param x 1 is right xDirection -1 is left
     * @param y 1 is down xDirection -1 is up
     * @throws PlayerException
     */
    public void move(int x, int y) throws PlayerException {
        assert x == 0 || x == -1 || x == 1;
        assert y == 0 || y == -1 || y == 1;
        assert (x != 0 || y != 0) && (x == 0 || y == 0);
        if (!game.isValidPosition(positionX + x, positionY + y)) {
            throw new PlayerException("You wanted to go to an invalid Position");
        }
        game.getTile(positionX, positionY).leave();
        positionX += x;
        positionY += y;
        game.getTile(positionX, positionY).enter(this);


    }

    public String getSign() {

        return sign;
    }

    public String getName() {

        return name;
    }

    public int getPositionY() {

        return positionY;
    }

    public String getGoalSide() {

        return goalSide;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((goalSide == null) ? 0 : goalSide.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + positionX;
        result = prime * result + positionY;
        result = prime * result + ((sign == null) ? 0 : sign.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Player other = (Player) obj;
        if (goalSide == null) {
            if (other.goalSide != null) {
                return false;
            }
        } else if (!goalSide.equals(other.goalSide)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (positionX != other.positionX) {
            return false;
        }
        if (positionY != other.positionY) {
            return false;
        }
        if (sign == null) {
            if (other.sign != null) {
                return false;
            }
        } else if (!sign.equals(other.sign)) {
            return false;
        }
        return true;
    }

    public int getPositionX() {

        return positionX;
    }

    @Override
    public String toString() {
        return "Player [positionX=" + positionX + ", positionY=" + positionY + ", goalSide=" + goalSide + ", sign="
                + sign + ", name=" + name + "]";
    }

    /**
     * Says how many walls the players is still allowed to place
     *
     * @return numbers of walls left to be placed by the player
     */
    public int getRemainingWalls() {

        return remainingWalls;
    }

    /**
     * Reduces the amount of walls this player can place if the number was bigger than 0 before
     */
    public void reduceRemainingWalls() {
        assert remainingWalls > 0;
        remainingWalls -= 1;


    }

    /**
     * Placing two wall tiles on tiles that where previously empty, Player must
     * have more than 0 walls left to place, also tiles must be right next to
     * each other.
     *
     * @param x1 xPosition of the first tile
     * @param y1 yPosition of the first tile
     * @param x2 xPosition of the second tile
     * @param y2 yPosition of the second tile
     * @throws PlayerException when one of the above conditions is not met
     */
    public void placeWalls(int x1, int y1, int x2, int y2) throws PlayerException {
        if (!(remainingWalls > 0)) {
            throw new PlayerException("You have already placed all of your Walls");
        }
        if (!(game.isValidPosition(x1, y1) && game.isValidPosition(x2, y2))) {
            throw new PlayerException("One or both of the specified tiles are not valid");
        }
        if (!(game.getTile(x1, y1).isAdjacentTile(game.getTile(x2, y2)))) {
            throw new PlayerException("The two tiles are not adjacent to each other");
        }
        game.setWall(x1, y1);
        game.setWall(x2, y2);
        if (!game.checkIfvalidPathsExists()) {

            game.unsetWall(x1, y1);
            game.unsetWall(x2, y2);
            throw new PlayerException("It was no longer possible to reach the End for one or more Players");
        }
        remainingWalls -= 1;

    }

    /**
     * Returns true if the tile is a winning tile
     *
     * @param tile must not be null
     * @return boolean
     */
    public boolean isWinningTile(Tile tile) {
        if (winningTiles.contains(tile)) {
            return true;
        }
        return false;
    }

    /**
     * Reads one line from the input and tries to make and execute from it
     *
     * @throws PlayerException if the read line cannot be parsed to a valid move
     * @throws IOException
     */
    public void makeMove() throws PlayerException, IOException {
        String move = input.readLine();
        //parseAndExecute(move);
        new Command(this, move, game);

    }

    /**
     * Parse the given String into a move and executes it
     *
     * @param move should be either (D|L|R|U) or x1 y1 x2 y2 (with x1,x2,y1,y2
     *             being integers)
     * @throws PlayerException if the move was either impossible to parse or execute
     */
    private void parseAndExecute(String move) throws PlayerException {
        switch (move) {
            case "D":
                move(0, 1);
                return;
            case "L":
                move(-1, 0);
                return;
            case "R":
                move(1, 0);
                return;
            case "U":
                move(0, -1);
                return;
        }
        int[] array = splitOnSpace(move);
        placeWalls(array[0], array[1], array[2], array[3]);
    }

    /**
     * Helper methods separates on space string should contain 4 numbers
     * seperated by space
     *
     * @param s should be x1 y1 x2 y2
     * @return int[] containing x1,y1,x2,y2 in that order
     * @throws PlayerException when one was not a number or their were not 4 numbers in the
     *                         string
     */
    protected static int[] splitOnSpace(String s) throws PlayerException {
        String[] word = s.split("\\s");
        assert word.length > 0;
        int[] numbers = new int[word.length];
        for (int i = 0; i < word.length; i++) {
            numbers[i] = Integer.parseInt(word[i]);
        }
        if (numbers.length != 4) {
            throw new PlayerException("If you want to place walls it should be x1 y1 x2 y2 and nothing else");
        }

        return numbers;

    }

    /**
     * Reads the last line from the BufferedInputReader input
     * @return String
     * @throws IOException
     */

    public String getInput() throws IOException {

        return input.readLine();
    }

}
