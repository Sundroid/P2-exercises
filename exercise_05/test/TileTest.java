import exercise05.Game;
import exercise05.IGame;
import exercise05.Player;
import exercise05.Tile;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TileTest {

    @Test(expected = AssertionError.class)
    public void noPlayerOnTileWhenLeave() {
        Tile tile = new Tile(false);
        tile.leave();

    }

    @Test(expected = AssertionError.class)
    public void PlayerOnTileWhenEnter() {
        Tile tile = new Tile(false);
        Player player = mock(Player.class);
        tile.enter(player);
        tile.enter(player);

    }

    @Test
    public void testEnter() {
        Tile tile = new Tile(false);
        Player player = mock(Player.class);
        tile.enter(player);
        assertEquals(player, tile.getPlayer());
    }

    @Test
    public void testLeave() {
        Tile tile = new Tile(false);
        Player player = mock(Player.class);
        tile.enter(player);
        tile.leave();
        assertEquals(null, tile.getPlayer());
    }

    @Test
    public void testGetContent() {
        Tile tile = new Tile(false);
        Player player = mock(Player.class);
        tile.enter(player);
        when(player.getSign()).thenReturn("D");
        assertEquals("D", tile.getContent());
    }

    @Test
    public void testGetContentEmpty() {
        Tile tile = new Tile(false);
        assertEquals(" ", tile.getContent());
    }

    @Test
    public void canBeEnterNoWallNoPlayer() {
        Tile tile = new Tile(false);
        assertTrue(tile.canBeEntered());
    }

    @Test
    public void canBeEnterfalseBecausePlayer() {
        Tile tile = new Tile(false);
        Player player = mock(Player.class);
        tile.enter(player);
        assertFalse(tile.canBeEntered());
    }

    @Test
    public void canBeEnterfalseBecauseWall() {
        Tile tile = new Tile(true);
        assertFalse(tile.canBeEntered());
    }

    @Test
    public void testAdjacentTile() {
        IGame game = mock(Game.class);
        Tile tile = new Tile(false, 2, 2, game);
        Tile tileR = new Tile(false, 3, 2, game);
        Tile tileL = new Tile(false, 1, 2, game);
        Tile tileU = new Tile(false, 2, 1, game);
        Tile tileD = new Tile(false, 2, 3, game);
        Tile wrongTile = new Tile(false, 4, 4, game);
        when(game.isValidPosition(anyInt(), anyInt())).thenReturn(true);
        when(game.getTile(3, 2)).thenReturn(tileR);
        when(game.getTile(1, 2)).thenReturn(tileL);
        when(game.getTile(2, 3)).thenReturn(tileD);
        when(game.getTile(2, 1)).thenReturn(tileU);
        when(game.getTile(4, 4)).thenReturn(wrongTile);
        assertFalse(tile.isAdjacentTile(4, 4));
    }

    @Test
    public void testAdjacentTileTrue() {
        IGame game = mock(Game.class);
        Tile tile = new Tile(false, 2, 2, game);
        Tile tileR = new Tile(false, 3, 2, game);
        Tile tileL = new Tile(false, 1, 2, game);
        Tile tileU = new Tile(false, 2, 1, game);
        Tile tileD = new Tile(false, 2, 3, game);
        when(game.isValidPosition(anyInt(), anyInt())).thenReturn(true);
        when(game.getTile(3, 2)).thenReturn(tileR);
        when(game.getTile(1, 2)).thenReturn(tileL);
        when(game.getTile(2, 3)).thenReturn(tileD);
        when(game.getTile(2, 1)).thenReturn(tileU);

        assertTrue(tile.isAdjacentTile(2, 3));
    }

}
