package turtle;

import java.util.List;
/**
 * Responsible for filling the board according to the user input commands
 * Starts the parser and executes moves
 * posX and posY should be between 0 and SIZE at all times
 * @author Tobias
 *
 */
public class BoardMaker {
	private boolean[][] board;
	private final static int SIZE = 100;
	private int posX,posY;
	
	/**
	 * Parse the given turtle program and evaluate it. Render the trail as
	 * described in the problem description and return a SIZExSIZE board
	 * corresponding to the evaluated path.
	 *
	 * @param turtleProgram input program according to specification. may also contain invalid text!
	 * @return SIZExSIZE boolean board, where true values denote "red trail".
	 * 
	 * --@Silas: you should state when and why the ParserException is thrown, this is part if that method's contract
	 */
	public boolean[][] makeBoardFrom(String turtleProgram) throws ParserException {

		board=initialBoard();
		CommandParser parser=new CommandParser(turtleProgram);
		List<Move> l; //@Silas: you should use more descriptive variable names
		try{l=parser.parse();}
		catch(Exception e){
			ParserException p=new ParserException(); //@Silas: actually, you can just say "throw new ParserException();"
			throw p;
			
		}
		applyMoves(l);
		checkInvariance();
		return board;
	}
	/**
	 * Executes all valid moves in a list of moves
	 * @param l Your List of moves
	 */
	public void applyMoves(List<Move> l){
		for(Move m:l){
			if(isValidMove(m))
			applyMove(m);
		}
		checkInvariance();
	} 
	
	/**
	 * Executes a single move, precondition: Move must be valid @Silas: when is it valid? when not? where can I look this up?
	 * postcondition: PosX, PosY are valid positions on the board
	 * @param m Your move @Silas you could put the precondition for m right here (again, maybe use a more descriptive name next time)
	 */
	public void applyMove(Move m){
		assert (posX+m.xMove<SIZE&&posY+m.yMove<SIZE);
		assert (posX+m.xMove>=0&&posY+m.yMove>=0);
		m.makeMove(posX,posY,this);
		posX=posX+m.xMove;
		posY=posY+m.yMove;
		checkInvariance();
		
	}
	/**
	 * Fills the described place on the board
	 * Precondition 0<x<SIZE 0<y<SIZE
	 * @param x x-position on the board
	 * @param y y-position on the board
	 */
	public void fill(int x,int y){ //@Silas: this should be private
		assert(x<SIZE&&y<SIZE);
		assert(y>=0&&x>=0);
		board[x][y]=true;
		
	}
	/**
	 * Checks if the posX and posY are still between 0 and SIZE
	 * @return True if conditions meet
	 */
	private void checkInvariance(){
		assert (posX<SIZE&&posY<SIZE);
		assert (posX>=0&&posY>=0);
	}
	/**
	 * Checks if a move is possible at the moment 
	 * @param m The move you want to check
	 * @return True if the move can be performed without leaving the board
	 */
	public boolean isValidMove(Move m){ //@Silas: this should be private
		return ((posX+m.xMove<SIZE&&posY+m.yMove<SIZE)&&(posX+m.xMove>=0&&posY+m.yMove>=0));
		
	}
	/**
	 * Create a new board and return it.
	 * @return board, must be of size SIZExSIZE.
	 */
	public boolean[][] initialBoard() {
		boolean[][] board=new boolean[SIZE][SIZE];
		posX=0;posY=0;
		return board;
	}
}