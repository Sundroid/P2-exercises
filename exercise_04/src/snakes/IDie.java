package snakes;

/**
 * Created by julian on 3/20/17.
 */
public interface IDie {
    int faces = 6;
    int roll();
}
