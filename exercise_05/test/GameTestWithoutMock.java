import exercise05.Game;
import exercise05.IGame;
import exercise05.Player;
import exercise05.PlayerException;
import exercise05.Tile;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class GameTestWithoutMock {
    protected IGame game;

    @Before
    public void setUp() throws PlayerException {
        List<String[]> players = createPlayers();
        game = new Game(3, 3, players);

    }

    private List<String[]> createPlayers() {
        List<String[]> playerString = new ArrayList<String[]>();
        String[] string = new String[5];
        string[0] = "John Doe";
        string[1] = "J";
        string[2] = "2";
        string[3] = "3";
        string[4] = "R";
        playerString.add(string);
        String[] string2 = new String[5];
        string2[0] = "John Doe";
        string2[1] = "K";
        string2[2] = "2";
        string2[3] = "1";
        string2[4] = "R";
        playerString.add(string2);
        return playerString;
    }

    @Test
    public void test() {
        assertEquals("K", game.getTileContent(2, 1));

    }

    @Test
    public void allOuterTilesAreWalls() {
        for (int i = 0; i < game.getWidth(); i++) {
            assertEquals(game.getTile(i, 0).getContent(), "#");
        }
        for (int i = 0; i < game.getWidth(); i++) {
            assertEquals(game.getTile(i, game.getHeight() - 1).getContent(), "#");
        }
        for (int i = 0; i < game.getHeight(); i++) {
            assertEquals(game.getTile(0, i).getContent(), "#");
        }
        for (int i = 0; i < game.getHeight(); i++) {
            assertEquals(game.getTile(game.getWidth() - 1, 0).getContent(), "#");
        }
    }

    @Test
    public void testValidPostion() {
        assertTrue(game.isValidPosition(3, 3));

    }

    @Test
    public void testValidPostionFalse() {
        assertFalse(game.isValidPosition(3, 4));

    }

    @Test
    public void testleftSideTiles() throws PlayerException {
        List<Tile> LeftSideTiles = new ArrayList<Tile>();
        for (int i = 1; i < game.getHeight() - 1; i++) {
            LeftSideTiles.add(new Tile(false, 1, i));
        }
        assertEquals(LeftSideTiles, game.getSideTiles("L"));
    }

    @Test
    public void testRightSideTiles() throws PlayerException {
        List<Tile> rightSideTiles = new ArrayList<Tile>();
        for (int i = 1; i < game.getHeight() - 1; i++) {
            rightSideTiles.add(new Tile(false, game.getWidth() - 2, i));
        }
        assertEquals(rightSideTiles, game.getSideTiles("R"));
    }

    @Test
    public void currentPlayerNextTest() {
        Player player = game.getCurrentPlayer();
        game.nextPlayer();
        assertNotEquals(game.getCurrentPlayer(), player);
    }

    @Test
    public void testUpSideTiles() throws PlayerException {
        List<Tile> upSideTiles = new ArrayList<Tile>();
        for (int i = 1; i < game.getWidth() - 1; i++) {
            upSideTiles.add(new Tile(false, i, 1));
        }
        assertEquals(upSideTiles, game.getSideTiles("U"));
    }

    @Test
    public void testdownSideTiles() throws PlayerException {
        List<Tile> downSideTiles = new ArrayList<Tile>();
        for (int i = 1; i < game.getHeight() - 1; i++) {
            downSideTiles.add(new Tile(false, i, game.getHeight() - 2));
        }
        assertEquals(downSideTiles, game.getSideTiles("D"));
    }

    @Test
    public void TestcanReachGoal() {
        assertTrue(game.checkIfvalidPathsExists());

    }

    @Test(expected = PlayerException.class)
    public void PlayerCantMoveUp() throws PlayerException {
        game.nextPlayer();
        game.getCurrentPlayer().move(0, -1);
    }

}