package snakes;
/*
 * Behaves like a regular square 
 * Except that players can randomly land on one of these after landing on Wormhole Entrance
 * 
 *
 */
public class WormholeExit extends Square {

	public WormholeExit(Game game, int position) {
		super(game, position);
	}
	@Override
	public boolean isWormholeExit() {
		return true;
	}


	@Override
	public String squareLabel() {
		return Integer.toString(position)+" (Exit)";
	}
}