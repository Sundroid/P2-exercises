IMHO it doesn't really matter, if you use Mockito or define a MockDie. At least not as long it's only about one method.
I think the overview is just better with mockito, because you can define the behaviour of your mocked Object  right away
in the Testing method, while you sometimes just paraphrase the object to be tested when implementing e.g. a MockDie.
Or said differently: If you do not use Mockito, you (have to) develop a simplified model of the code you want to test.