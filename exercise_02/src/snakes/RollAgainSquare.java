package snakes;
//If a player lands on here he rolls the die again bringing him to another square
//The square he's brought to has to handle the rest.

public class RollAgainSquare extends Square {
	public RollAgainSquare(Game game, int position) {
		super(game, position);
	}
	@Override
	public ISquare landHereOrGoHome(){
		return super.moveAndLand(game.getDie().roll());
	}
}