package turtle;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import javafx.util.Pair;

public class CommandParserTest {

	@Test 
	public void testSouthEast() {
		List<String> words2=new ArrayList<>();
		words2.add("south");
		words2.add("east");
		assertEquals(new Pair<Integer,Integer>(1,1),Move.findDirection(words2));
	}
	
	@Test
	public void test34() {
		//Tests method that no longer exists.
		/*CommandParser parser;
		
		parser = new CommandParser("34");
		assertEquals(34, parser.parseNumber());
		
		//parser = new CommandParser("  34"); // not allowed: parseNumber expects initial number TODO should that be a test expecting an AssertionError? Should there be such tests?
		//assertEquals(34, parser.parseNumber());

		parser = new CommandParser("34RRrrR");
		assertEquals(34, parser.parseNumber());*/
	}
	

	@Test
	public void testSplitOnSpace() {
		List<String> words=new ArrayList<>();
		words.add("north");
		words.add("south");
		
		assertEquals(words,CommandParser.splitOnSpace("north south"));
	}

	@Test
	public void testAdvanceWhileNumberOrWhitespace() {
		CommandParser p = new CommandParser("south 4 East 4");
										   //012345678
										   //      ^ ^ 6 to 8
		
		p.position = 6; // this is a WHITEBOX test!
		p.advanceWhileNumberOrWhiteSpace();
		assertEquals(8, p.position);
	}

	@Test
	public void testParseNumbers() {
		List<Integer> numbers=new ArrayList<>();
		numbers.add(3);
		numbers.add(5);
		assertEquals(numbers,CommandParser.splitNumbers("3 5"));
	}
	

	@Test
	public void testidotdsag(){
		/*Can no longer work
		 * CommandParser parser=new CommandParser("South 5 3");
		List<Move> moves=new ArrayList<>();
		moves.add(new Move(0,5));
		assertEquals(moves,parser.parse());*/
		
		
	}
	
	@Test
	public void testidotdsa2g(){
		/*
		Can no longer work
		CommandParser parser=new CommandParser("South 5 3 East 3");
		List<Move> moves=new ArrayList<>();
		moves.add(new Move(0,5));
		moves.add(new Move(3,0));
		assertEquals(moves,parser.parse());
		
		*/
	}
	@Test
	public void test() {

		CommandParser parser2=new CommandParser("South West");
		
		/*Move m=new Move(0,5);
		
		assertEquals("North",parser.parseWord(0, "North2").getKey());
		assertEquals("North",parser.parseWord(" North2").getKey());
		assertEquals("North South",parser.parseWord(0,"North South").getKey());
		assertEquals("South East",parser.parseWord(0,"South East 5").getKey().trim());
		assertEquals("South ",parser.parseWord(0,"South 5").getKey());
		
		assertEquals(m,parser.parseMove(0, "South 5").getKey());
	
		List<Move> l = new ArrayList<Move>(); l.add(m);
		assertEquals(l,parser.parse("South 5"));
		l.add(new Move(-5,0));
			
		assertEquals(l,parser.parse("South 5 West 5"));
			return;*/
	}
	

}
