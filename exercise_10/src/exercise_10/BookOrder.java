package exercise_10;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import static exercise_10.Shop.shop;


public class BookOrder {
    private String clientName;
    private Calendar orderDate;
    private Batch books;
    private double price, finalPrice;
    private int numberOfBooks, inCategorySoftwareEngineering;
    private ArrayList<Batch> batchesToAdd;

    public BookOrder(String clientName, Calendar orderDate) {
        this.clientName = clientName;
        this.orderDate = orderDate;
        this.books = new Batch(null);
        price = 0;
        finalPrice = 0;
        numberOfBooks = 0;
        batchesToAdd = new ArrayList<>();
    }


    /**
     * Adds a book to an order
     * @param book the book you want to add
     * @param copies the number of copies of the book to be added
     */

    public void addBook(Book book, int copies) {
        assert (book != null);
        assert (shop.get(book) >= copies);
        this.books.addBook(book, copies);
        int tmp = shop.remove(book);
        shop.addBook(book, tmp - copies);
    }

    /**
     * adds a batch to an order, checks if there are enough copies available in the shop.
     * @param batch the batch you want to add (any, no matter if created yourself or prepared from the shop)
     */


    public void addBatch(Batch batch) {
        for (HashMap.Entry<Book, Integer> entry : batch.entrySet()) {
            assert (shop.get(entry.getKey()) >= entry.getValue());
        }
        for (HashMap.Entry<Book, Integer> entry : batch.entrySet()) {
            int tmp = shop.remove(entry.getKey());
            shop.addBook(entry.getKey(), tmp - entry.getValue());
            this.addBook(entry.getKey(), entry.getValue());
        }
        this.books.updateNestedBatches(batch);
    }

    /**
     * Let's you create a new batch through the order, gets added to the order automatically
     * @param name the name of the batch
     * @return returns the freshly created batch
     */

    public Batch newBatch(String name) {
        Batch batch = new Batch(name);
        this.batchesToAdd.add(batch);
        return batch;
    }

    /**
     * Adds the batches created through the order and thus modified after being added to the order on call
     */
    private void addBatches(){
        for (Batch batch: batchesToAdd){
            this.addBatch(batch);
        }
    }


    /**
     *
     * @return a short form of the order
     */
    public String summary() {
        addBatches();
        StringBuilder summary = new StringBuilder();
        computeFinalPrice();
        summary.append(header(true));
        summary.append(books.toString(true));
        return summary.toString();
    }

    /**
     *
     * @return a lengthy form of the order
     */
    public String details() {
        addBatches();
        StringBuilder details = new StringBuilder();
        computeFinalPrice();
        details.append(header(false));
        details.append(books.toString(false));
        return details.toString();
    }

    /**
     *
     * @param isSummary pick header details/length
     * @return header as string
     */
    // Idk, if this is how a template method works. I unfortunately was unable to find a proper description.
    private String header(boolean isSummary) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy");
        StringBuilder header = new StringBuilder();
        header.append("Date: " + sdf.format(orderDate.getTime()) + "\n");
        header.append("Client: " + clientName + "\n");
        header.append("Number of books: " + numberOfBooks + "\n");
        if (!isSummary) {
            header.append("Total Price: " + price + " CHF\n");
            header.append("Discount: " + getDiscount() + " CHF\n");
        }
        header.append("Final Price: " + finalPrice + " CHF\n\n");
        if (isSummary) {
            header.append("Books\n");
        }

        return header.toString();
    }

    /**
     * computes the final price
     */

    private void computeFinalPrice() {
        for (HashMap.Entry<Book, Integer> entry : books.entrySet()) {
            price += entry.getKey().getPrice() * entry.getValue();
            numberOfBooks += entry.getValue();
            if (entry.getKey().getCategory() != null && entry.getKey().getCategory().equals("Software Engineering")) {
                inCategorySoftwareEngineering += entry.getValue();
            }
        }

        //Kinda obvious Chain of responsibility.
        finalPrice = price;
        if (numberOfBooks >= 10) {
            finalPrice *= 0.9;
        }
        if (finalPrice > 1000) {
            finalPrice -= 90;
        }
        if (inCategorySoftwareEngineering >= 5) {
            finalPrice *= 0.95;
        }
        if (orderDate.get(Calendar.MONTH) == Calendar.MAY) {
            finalPrice *= 0.95;
        }
    }

    /**
     * Calculates the effective discount on the order
     * @return the discount (difference between ordinary- and endprice
     */
    private double getDiscount() {
        return price - finalPrice;
    }

}

