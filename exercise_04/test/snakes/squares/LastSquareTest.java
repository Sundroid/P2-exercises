package snakes.squares;

import org.junit.Test;
import snakes.Game;
import snakes.squares.*;
import snakes.squares.StandardSquare;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import org.junit.Test;

public class LastSquareTest extends StandardSquareTest {

	@Test
	public void isLastSquare() {
		Game game = mock(Game.class);
		when(game.isValidPosition(anyInt())).thenReturn(true);
		Square lastSquare=new LastSquare(game,1);
		assertTrue(lastSquare.isLastSquare());
	}

}
