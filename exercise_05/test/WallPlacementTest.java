import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;

import org.junit.Before;
import org.junit.Test;

import exercise05.IGame;
import exercise05.Parser;
import exercise05.PlayerException;
import exercise05.TestServiceLocator;

public class WallPlacementTest {

    private IGame game;

    @Before
    public void scenarioStart() throws IOException, PlayerException {

    }

    @Test
    public void cuttingOfPartofBoardWorks() throws PlayerException, IOException {
        StringBuilder moves = new StringBuilder();
        moves.append("D\n");
        moves.append("2 2 2 1\n");
        moves.append("2 0 1 0\n");
        TestServiceLocator locator = new TestServiceLocator();
  		locator.setInput(new BufferedReader(new StringReader(moves.toString())));
        BufferedReader br = new BufferedReader(new FileReader("games/game3.txt"));
        Parser parse = new Parser();
        game = parse.parse(br, new BufferedReader(new StringReader(moves.toString())));


        game.nextTurn();
        game.nextTurn();
        game.nextTurn();

        assertEquals("#", game.getTileContent(2, 2));
        assertEquals("#", game.getTileContent(2, 1));
        assertEquals("#", game.getTileContent(2, 0));
        assertEquals("#", game.getTileContent(1, 0));
    }

    @Test(expected = PlayerException.class)
    public void blockingDoesNot() throws PlayerException, IOException {
    	
        StringBuilder moves = new StringBuilder();

        moves.append("3 3 4 3\n");
        moves.append("5 3 5 2\n");
        TestServiceLocator locator = new TestServiceLocator();
		locator.setInput(new BufferedReader(new StringReader(moves.toString())));
        
        BufferedReader br = new BufferedReader(new FileReader("games/game3.txt"));
        Parser parse = new Parser();
        game = parse.parse(br, new BufferedReader(new StringReader(moves.toString())));


        game.nextTurn();
        game.nextTurn();
    }

    @Test
    public void placingAWallNormalyDoes() throws PlayerException, IOException {
        StringBuilder moves = new StringBuilder();
        

        moves.append("2 2 2 1\n");
        TestServiceLocator locator = new TestServiceLocator();
		locator.setInput(new BufferedReader(new StringReader(moves.toString())));
      
        BufferedReader br = new BufferedReader(new FileReader("games/game3.txt"));
        Parser parse = new Parser();
        game = parse.parse(br, new BufferedReader(new StringReader(moves.toString())));


        game.nextTurn();
        assertEquals("#", game.getTileContent(2, 2));
        assertEquals("#", game.getTileContent(2, 1));
    }


}
