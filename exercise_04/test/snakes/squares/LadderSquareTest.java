package snakes.squares;

import org.junit.Before;
import org.junit.Test;
import snakes.Game;
import snakes.Player;
import snakes.squares.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

public class LadderSquareTest extends StandardSquareTest {

	@Test
	public void testMoveAndLandOnly() {
		Square start, stop;
		Square testSquare = new LadderSquare(1,game, 1);
		start = mock(Square.class);
		stop = mock(Square.class);

		when(game.findSquare(1, 2)).thenReturn(start);
		when(start.landHereOrGoHome()).thenReturn(stop);

		Square destination = testSquare.moveAndLand(2);
		assertEquals(stop, destination);
	}
	@Test
	public void LandHereOrGoHomeNotOccupied() {
		
		Square testSquare=new LadderSquare(1,game,1);
		Square endSquare=mock(Square.class);
		when(game.getSquare(2)).thenReturn(endSquare);
		Square firstSquare=mock(Square.class);
		when(game.firstSquare()).thenReturn(firstSquare);
		Square destination = testSquare.landHereOrGoHome();
		assertNotEquals(firstSquare,destination);
	}

	@Test
	public void LandHereOrGoHomeOccupied() {
		
		Square endSquare=new StandardSquare(game,1);
		Square testSquare=new LadderSquare(1,game,1);
		Square firstSquare=mock(Square.class);
		when(game.firstSquare()).thenReturn(firstSquare);
		when(game.getSquare(2)).thenReturn(endSquare);
		Player player=mock(Player.class);
		testSquare.enter(player);
		Square destination = testSquare.landHereOrGoHome();
		assertEquals(endSquare,destination);
	}
}
