package exercise05;

import java.io.IOException;
import java.util.List;

public interface IGame {

	String toString();

	int hashCode();

	boolean equals(Object obj);

	/**
	 * Gives you the content of a tile,is one 1 character long string
	 * Visually represent the tile
	 *
	 * @param x xPosition of your tile 0<=x<width
	 * @param y yPosition of your tile 0<=y<heigth
	 * @return 1 character long string
	 */
	String getTileContent(int x, int y);

	int getHeight();

	int getWidth();

	/**
	 * Calls the enter Player method of the specified tile
	 *
	 * @param x x should be between 1 and width-1 1<=x<width-1
	 * @param y y should be between 1 and height-1 1<=x<height-1
	 */
	void enterPlayerOnTile(int x, int y);

	/**
	 * Check if a position is inRange and if a player can step on the tile their
	 */
	boolean isValidPosition(int x, int y);

	/**
	 * Getting the tile on this position Position must be inRange
	 */
	Tile getTile(int x, int y);

	/**
	 * Check for all player if they can still reach the end or not
	 *
	 * @return false if one player can not reach his goal side
	 */
	boolean checkIfvalidPathsExists();

	/**
	 * Getting all tiles on the Side you have described
	 *
	 * @param goalSide must be R L U D for right,left,up,down
	 * @return Tile list on this side
	 * @throws PlayerException
	 */
	List<Tile> getSideTiles(String goalSide) throws PlayerException;

	/**
	 * Sets the tile at described position to a wall Precondition tile must
	 * exist
	 *
	 * @param x xPosition of your Tile
	 * @param y yPosition of your Tile
	 */
	void setWall(int x, int y);

	/**
	 * Unsets the tile at described position so its no longer a wall
	 * Precondition tile must exist
	 *
	 * @param x xPosition of your Tile
	 * @param y yPosition of your Tile
	 */
	void unsetWall(int x, int y);

	/**
	 * Checks if game is over for the currentPlayer
	 *
	 * @return true when currentPlayer won
	 */
	boolean IsOver();

	/**
	 * Rotates to the nextPlayer
	 */
	void nextPlayer();

	/**
	 * Lets the currentPlayer do one turn and changes the currentPlayer if
	 * currentPlayer did not win
	 *
	 * @throws PlayerException if move made by the player was incorrect
	 * @throws IOException
	 */
	void nextTurn(UserInteraction interactor);

	void nextTurn() throws PlayerException, IOException;

	Player getCurrentPlayer();

	void play(UserInteraction interactor);

	List<Player> getPlayers();

}