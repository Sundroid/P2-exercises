package exercise05;

import java.io.BufferedReader;
import java.io.PrintStream;

public abstract class ServiceLocator {
	private static ServiceLocator instance;
	public static ServiceLocator instance() {
		if (instance == null) {
			instance = defaultServiceLocator();
		}
	return instance;
	}
	public static ServiceLocator defaultServiceLocator() {
		return new DefaultServiceLocator();
	}
	public abstract PrintStream getOutput() ;
	public abstract UserInteraction getUserInteractor();
	public abstract IGame getIGame();
	public abstract BufferedReader getInput();
	public static void setLocator(ServiceLocator serviceLocator) {
		instance = serviceLocator;
	}
		

}
