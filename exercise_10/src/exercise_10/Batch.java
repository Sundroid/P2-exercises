package exercise_10;

import java.util.ArrayList;
import java.util.HashMap;


public class Batch extends HashMap<Book, Integer> {
    private String name;
    private ArrayList<Batch> nestedBatches;
    private static StringBuilder sb;


    /**
     * Creates a new batch
     *
     * @param name the name you want the batch to have
     */
    public Batch(String name) {
        super();
        nestedBatches = new ArrayList<Batch>();
        this.name = name;
    }

    @Override
    public Integer put(Book book, Integer quantity) {
        assert (book != null && quantity != null);
        if (this.containsKey(book)) {
            int temp = this.get(book) + quantity;
            this.replace(book, temp);
        } else {
            super.put(book, quantity);
        }
        return this.get(book);
    }

    /**
     * Adds a book with any quantity to a batch
     *
     * @param book     the book you want to add
     * @param quantity the number of copies to be added
     */

    public void addBook(Book book, int quantity) {
        assert (book != null && quantity > 0);
        this.put(book, quantity);
    }

    /**
     * Helper method to keep track of books that get added to an order through a Batch which was modified, after it has been added to the order
     *
     * @param batch the batch which was modified
     */

    public void updateNestedBatches(Batch batch) {
        nestedBatches.add(batch);
    }

    /**
     * add one copy of a book
     *
     * @param book the want to add
     */

    public void addBook(Book book) {
        assert (book != null);
        this.put(book, 1);
    }

    /**
     * Allow to add a Batch to a batch (nested batches), adds all books contained to the toplayer batch
     *
     * @param batch the batch you want to add
     */
    public void addBatch(Batch batch) {
        assert (batch != null);
        for (HashMap.Entry<Book, Integer> entry : batch.entrySet()) {
            this.addBook(entry.getKey(), entry.getValue());
        }
        nestedBatches.add(batch);
    }

    /**
     * @return the name of the batch
     */

    public String getName() {
        return name;
    }


    /**
     * Changes the name of a batch
     * @param name the new name of a batch
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * to string method of a batch, kind of missused
     * @param isSummary let's you get a short form of the batch
     * @return the batch as string
     */

    public String toString(boolean isSummary) {
        if (isSummary) {
            StringBuilder summary = new StringBuilder();
            int bookNumber = 1;
            for (HashMap.Entry<Book, Integer> entry : this.entrySet()) {
                summary.append(bookNumber + ".");
                summary.append(entry.getKey().toString(true) + "\n");
                summary.append("\tQuantity: " + entry.getValue());
            }
            return summary.toString();

        } else {
            sb = new StringBuilder();
            printNestedBatches(this);
            return sb.toString();
        }
    }

    /**
     * recursive way to print nested batches
     * @param batch the toplayer batch from which you want to recurse from (usually "this")
     */

    private void printNestedBatches(Batch batch) {
        if (batch.nestedBatches.isEmpty()) {
            sb.append("Batch '" + batch.name + "'\n\n");
            for (HashMap.Entry<Book, Integer> entry : batch.entrySet()) {
                sb.append(entry.getKey().toString(false) + "\n");
                sb.append("\tQuantity: " + entry.getValue() + "\n\n");
            }
        } else {
            for (int i = 0; i < batch.nestedBatches.size(); i++) {
                printNestedBatches(batch.nestedBatches.get(i));
            }
        }
    }
}

