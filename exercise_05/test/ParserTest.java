import exercise05.Game;
import exercise05.IGame;
import exercise05.Parser;
import exercise05.PlayerException;
import exercise05.Renderer;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ParserTest {

    @Test
    public void test() throws IOException {
        Parser parser = new Parser();
        parser.parsePlayer("Otis Redding O 1 1 R", 5);
        List<String[]> player = new ArrayList<String[]>();
        String[] playerString = new String[5];
        playerString[0] = "Otis Redding";
        playerString[1] = "O";
        playerString[2] = "1";
        playerString[3] = "1";
        playerString[4] = "R";
        player.add(playerString);
        assertTrue(Arrays.equals(player.get(0), parser.players.get(0)));
    }

    @Test
    public void test2() throws IOException {
        Parser parser = new Parser();
        parser.parsePlayer("Otis Redding O 2 2 R", 5);
        List<String[]> player = new ArrayList<String[]>();
        String[] playerString = {"Otis Redding", "O", "2", "2", "R"};
        player.add(playerString);
        assertTrue(Arrays.equals(player.get(0), parser.players.get(0)));
    }

    @Test(expected = Throwable.class)
    public void testWrongInput() throws IOException {
        Parser parser = new Parser();
        parser.parsePlayer("Otis Redding O2 R", 5);
        List<String[]> player = new ArrayList<String[]>();
        String[] playerString = {"Otis Redding", "O", "2", "2", "R"};
        player.add(playerString);
        assertTrue(Arrays.equals(player.get(0), parser.players.get(0)));
    }

    @Test
    public void test3() throws IOException, PlayerException {
        BufferedReader br = new BufferedReader(new FileReader("games/game1.txt"));

        Parser parser = new Parser();
        List<String[]> player = new ArrayList<String[]>();
        String[] playerString = {"Otis Redding", "O", "1", "1", "R"};
        String[] playerString2 = {"Solomon Burke", "S", "7", "12", "L"};
        player.add(playerString);
        player.add(playerString2);
        IGame game = new Game(7, 12, player);
        IGame parsedGame = parser.parse(br, new BufferedReader(new InputStreamReader(System.in)));
        assertEquals(game, parsedGame);
    }

    @Test
    public void test4() throws IOException, PlayerException {
        BufferedReader br = new BufferedReader(new FileReader("games/game3.txt"));

        Parser parser = new Parser();
        List<String[]> player = new ArrayList<String[]>();
        String[] playerString = {"O", "Otis Reading"};
        String[] playerString2 = {"S", "Solomon Burke"};
        player.add(playerString);
        player.add(playerString2);

        IGame parsedGame = parser.parse(br, new BufferedReader(new InputStreamReader(System.in)));
        //System.out.println(Renderer.draw(parsedGame));
        /*assertEquals("#sssss#\n" +
                     "#     #\n" +
					 "#  O  #\n" + 
					 "###   #\n" + 
					 "#     #\n" + 
					 "#   ###\n" + 
					 "#  S###\n" + 
					 "#ooo###",Renderer.draw(parsedGame));*/

        assertEquals(parsedGame.getHeight(), 8);
        assertEquals(parsedGame.getWidth(), 7);
        assertEquals(parsedGame.getCurrentPlayer().getPositionX(), 3);
        assertEquals(parsedGame.getCurrentPlayer().getPositionY(), 2);
    }


    @Test
    public void checkStat() throws IOException, PlayerException {
        Parser parser = new Parser();
        String string = "6 4 2 3\n"
                + "######\n"
                + "# s s#\n"
                + "# S  #\n"
                + "######\n"
                + "S S";
        List<int[][]> list = new ArrayList<int[][]>();
        int[][] a = {{1, 1, 1, 1, 1, 1}, {1, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 1}, {1, 1, 1, 1, 1, 1}};
        int[][] b = {{0, 0, 0, 0, 0, 0}, {0, 1, 0, 1, 0, 0}, {0, 1, 0, 1, 1, 0}, {0, 0, 0, 0, 0, 0}};
        int[][] d = {{0, 0, 0, 0, 0, 0}, {0, 0, 1, 0, 1, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}};
        int[][] c = {{0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 1, 0, 0, 0}, {0, 0, 0, 0, 0, 0}};
        list.add(a);
        list.add(b);
        list.add(d);
        list.add(c);
        List<Character> signs = new ArrayList<Character>();
        signs.add('#');
        signs.add(' ');
        signs.add('s');
        signs.add('S');
        BufferedReader reader = new BufferedReader(new StringReader(string));
        parser.parse(reader, reader);
        for (int i = 0; i < list.size(); i++) {
            assertEquals(parser.getUsages().get(i), list.get(i));
        }
        assertEquals(parser.getSigns(), signs);


    }

}

