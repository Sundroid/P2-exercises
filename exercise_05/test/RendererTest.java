import exercise05.Game;
import exercise05.IGame;
import exercise05.Renderer;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RendererTest extends GameTestWithoutMock {

    @Test
    public void renderTestEmpty() {
        IGame gameMock = mock(Game.class);

        when(gameMock.getWidth()).thenReturn(5);
        when(gameMock.getHeight()).thenReturn(6);
        when(gameMock.getTileContent(anyInt(), anyInt())).thenReturn(" ");

        when(gameMock.getTileContent(anyInt(), eq(5))).thenReturn("#");
        when(gameMock.getTileContent(eq(4), anyInt())).thenReturn("#");
        when(gameMock.getTileContent(anyInt(), eq(0))).thenReturn("#");
        when(gameMock.getTileContent(eq(0), anyInt())).thenReturn("#");

        String result = Renderer.draw(gameMock);
        assertEquals("#####\n" +
                "#   #\n" +
                "#   #\n" +
                "#   #\n" +
                "#   #\n" +
                "#####\n", result);
    }

    @Test
    public void renderTestWithPlayers() {
        IGame gameMock = mock(Game.class);

        when(gameMock.getWidth()).thenReturn(5);
        when(gameMock.getHeight()).thenReturn(5);

        when(gameMock.getTileContent(anyInt(), anyInt())).thenReturn(" ");

        when(gameMock.getTileContent(anyInt(), eq(4))).thenReturn("#");
        when(gameMock.getTileContent(eq(4), anyInt())).thenReturn("#");
        when(gameMock.getTileContent(anyInt(), eq(0))).thenReturn("#");
        when(gameMock.getTileContent(eq(0), anyInt())).thenReturn("#");


        when(gameMock.getTileContent(1, 1)).thenReturn("K");
        when(gameMock.getTileContent(1, 3)).thenReturn("J");
        String result = Renderer.draw(gameMock);
        assertEquals("#####\n" + "#K  #\n" + "#   #\n" + "#J  #\n" + "#####\n", result);
    }

    @Test
    public void renderTestWithPlayersGame() {

        String result = Renderer.draw(game);
        assertEquals("#####\n" + "# K #\n" + "#   #\n" + "# J #\n" + "#####\n", result);
    }

}
