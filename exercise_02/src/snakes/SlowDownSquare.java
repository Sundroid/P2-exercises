package snakes;
/*
 * Landing on here is like landing on a regular square
 * If a player is on this Square, every number rolled for movement is halved(round-up)
 */
public class SlowDownSquare extends Square {
	public SlowDownSquare(Game game, int position) {
		super(game, position);
	}
	@Override
	public ISquare moveAndLand(int moves){
		return super.moveAndLand((moves+1)/2);
	}

	@Override
	public String squareLabel() {
		return String.format("%d (SlowDown)", position);
	}
}
